package jobs

import (
	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/leader"
)

type Runner struct {
	leader.Leader
	leadingJobs  []BaseRunner
	standardJobs []BaseRunner
}

func (r *Runner) Init(standardJobs []BaseRunner, leadingJobs []BaseRunner) {
	r.leadingJobs = leadingJobs
	r.standardJobs = standardJobs
	log.Infof("starting gocron with %d standard task(s)", len(r.standardJobs))
	for _, j := range r.standardJobs {
		_ = gocron.Every(j.GetInterval()).Seconds().Do(j.Do)
	}
	gocron.Start()
}

func (r *Runner) OnElect() {
	log.Infof("starting gocron with %d task(s)", len(r.leadingJobs))
	for _, j := range r.leadingJobs {
		_ = gocron.Every(j.GetInterval()).Seconds().Do(j.Do)
	}
}

func (r *Runner) OnDemote() {
	log.Info("clearing gocron leading tasks as we have been demoted")
	for _, j := range r.leadingJobs {
		gocron.Remove(j.Do)
	}
}

type BaseRunner interface {
	GetInterval() uint64
	Do()
}
