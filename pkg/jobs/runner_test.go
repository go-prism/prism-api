package jobs

import (
	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/leader"
	"testing"
	"time"
)

// interface guard
var _ leader.Leader = &Runner{}

type testTask struct {
	count int
	BaseRunner
}

func (t *testTask) GetInterval() uint64 {
	return 1
}
func (t *testTask) Do() {
	t.count++
	log.Infof("hello: %d", t.count)
}

func TestRunner_Init(t *testing.T) {
	r := &Runner{}
	t1 := &testTask{}
	t2 := &testTask{}
	go r.Init([]BaseRunner{t1}, []BaseRunner{t2})

	time.Sleep(100 * time.Millisecond)
	assert.Equal(t, 1, len(gocron.Jobs()))
	// confirm that election starts leader jobs
	r.OnElect()
	time.Sleep(100 * time.Millisecond)
	assert.Equal(t, 2, len(gocron.Jobs()))

	// confirm that demotion stops leader jobs
	r.OnDemote()
	time.Sleep(100 * time.Millisecond)
	assert.Equal(t, 1, len(gocron.Jobs()))
}
