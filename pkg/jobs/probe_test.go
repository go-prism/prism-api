package jobs

import (
	"context"
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

// interface guard
var _ BaseRunner = &ProbeJob{}

func TestProbeJob_updateStatus(t *testing.T) {
	var cases = []struct {
		name     string
		status   *v1.RemoteStatus
		code     int
		expected v1.RemoteStatus_Status
	}{
		{
			"init stays unknown",
			&v1.RemoteStatus{},
			http.StatusOK,
			v1.RemoteStatus_UNKNOWN,
		},
		{
			"not-healthy becomes healthy",
			&v1.RemoteStatus{Healthy: 10, Status: v1.RemoteStatus_OUT_OF_SERVICE},
			http.StatusOK,
			v1.RemoteStatus_IN_SERVICE,
		},
		{
			"healthy becomes unhealthy",
			&v1.RemoteStatus{
				Healthy:   25,
				Unhealthy: 2,
				Status:    v1.RemoteStatus_IN_SERVICE,
			},
			http.StatusInternalServerError,
			v1.RemoteStatus_OUT_OF_SERVICE,
		},
	}

	pj := &ProbeJob{}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			pj.updateStatus(tt.status, tt.code)
			assert.Equal(t, tt.expected, tt.status.Status)
		})
	}
}

func TestProbeJob_doRequest(t *testing.T) {
	ts := httptest.NewServer(http.NotFoundHandler())
	defer ts.Close()

	pj := &ProbeJob{}
	resp, err := pj.doRequest(ts.URL)
	assert.NoError(t, err)
	defer resp.Body.Close()
	assert.Equal(t, http.StatusNotFound, resp.StatusCode)

	// ensure that a bad url returns an error
	_, err = pj.doRequest("")
	assert.Error(t, err)
}

func TestProbeJob_doRequest_timeout(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// take a long time to respond to force the client to timeout
		time.Sleep(6 * time.Second)
		_, _ = w.Write([]byte("OK"))
	}))

	pj := &ProbeJob{}
	_, err := pj.doRequest(ts.URL)
	assert.ErrorIs(t, err, context.DeadlineExceeded)
}
