package jobs

import (
	"context"
	"errors"
	"fmt"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/prometheus/client_golang/prometheus"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gorm.io/gorm"
	"math/rand"
	"net/http"
	"time"
)

var (
	metricResponseCode = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "remote_probe_response_code",
	}, []string{"code"})
	metricRemotesInService = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "remote_probe_in_service",
	})
	metricRemotesOutOfService = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "remote_probe_out_of_service",
	})
)

type ProbeJob struct {
	remoteRepo *repo.RemoteRepo
	statusRepo *repo.StatusRepo

	remoteList []string
	enabled    bool
}

func NewProbeJob(repos *repo.Repos, enabled bool) *ProbeJob {
	p := new(ProbeJob)
	p.statusRepo = repos.StatusRepo
	p.remoteRepo = repos.RemoteRepo
	p.enabled = enabled

	log.Infof("remote health checks: %v", enabled)

	return p
}

func (p *ProbeJob) refillPool() error {
	ids, err := p.remoteRepo.FindAllIDs(context.TODO())
	if err != nil {
		return err
	}
	// shuffle the list
	rand.Shuffle(len(ids), func(i, j int) {
		ids[i], ids[j] = ids[j], ids[i]
	})
	p.remoteList = ids
	log.Debugf("refilled probe pool with %d entries", len(p.remoteList))
	return nil
}

func (*ProbeJob) GetInterval() uint64 {
	return 5
}

func (p *ProbeJob) Do() {
	if !p.enabled {
		return
	}
	// make sure the pool is full
	if len(p.remoteList) == 0 {
		// if we can't refill, back out
		if err := p.refillPool(); err != nil {
			return
		}
		if len(p.remoteList) == 0 {
			return
		}
	}
	ctx := context.TODO()
	// pick a remote at random
	id, ids := p.remoteList[len(p.remoteList)-1], p.remoteList[:len(p.remoteList)-1]
	p.remoteList = ids
	fields := log.Fields{"remote": id}
	log.WithFields(fields).Infof("selected remote %s for probing from pool of %d", id, len(p.remoteList))
	// fetch the full object
	remote, err := p.remoteRepo.FindByID(ctx, id)
	if err != nil {
		return
	}
	log.WithFields(fields).Debugf("executing probe request to url: '%s'", remote.Uri)
	start := time.Now()
	resp, err := p.doRequest(remote.Uri)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to execute request")
		return
	}
	defer resp.Body.Close()
	log.WithFields(fields).Infof("remote '%s' responded with code %d", remote.Name, resp.StatusCode)
	metricResponseCode.WithLabelValues(fmt.Sprintf("%d", resp.StatusCode)).Inc()
	var status *v1.RemoteStatus
	existing, err := p.statusRepo.FindByRemoteID(ctx, remote.Id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			status = &v1.RemoteStatus{
				Id:       uuid.NewV4().String(),
				RemoteID: remote.Id,
				Status:   v1.RemoteStatus_UNKNOWN, // start unknown
			}
		} else {
			log.WithError(err).WithFields(fields).Error("failed to lookup existing status, bailing out...")
			return
		}
	} else {
		status = existing
	}
	status.Latency += time.Since(start).Milliseconds()
	status.Total++
	p.updateStatus(status, resp.StatusCode)
	_, _ = p.statusRepo.Save(ctx, status)
}

func (*ProbeJob) doRequest(uri string) (*http.Response, error) {
	// create a context that will time out after a few seconds
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*5)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, http.MethodHead, uri, nil)
	if err != nil {
		return nil, err
	}
	// execute the request
	resp, err := http.DefaultClient.Do(req)
	return resp, err
}

func (*ProbeJob) updateStatus(s *v1.RemoteStatus, code int) {
	// increment the healthy/unhealthy trackers
	if httputils.IsHTTPSuccess(code) {
		s.Healthy++
	} else {
		s.Unhealthy++
	}
	// check if we've tripped any gates
	if s.Healthy > 10 && s.Status != v1.RemoteStatus_IN_SERVICE {
		metricRemotesInService.Inc()
		if s.Status == v1.RemoteStatus_OUT_OF_SERVICE {
			metricRemotesOutOfService.Dec()
		}
		s.Status = v1.RemoteStatus_IN_SERVICE
		log.Info("remote state switched to IN_SERVICE")
		s.Unhealthy = 0
	}
	if s.Unhealthy > 2 && s.Status != v1.RemoteStatus_OUT_OF_SERVICE {
		metricRemotesOutOfService.Inc()
		if s.Status == v1.RemoteStatus_IN_SERVICE {
			metricRemotesInService.Dec()
		}
		s.Status = v1.RemoteStatus_OUT_OF_SERVICE
		log.Warning("remote state switched to OUT_OF_SERVICE")
		s.Healthy = 0
	}
}
