package v1

const (
	ArchetypeMaven   = "maven"
	ArchetypeNode    = "node"
	ArchetypeAlpine  = "alpine"
	ArchetypeGeneric = "generic"
	ArchetypeGo      = "golang"
	ArchetypeDebian  = "debian"
	ArchetypeHelm    = "helm"
)

// Archetype defines a known remote structure
// (e.g. Maven or NPM).
//
// Unknown structures may have undefined behaviour.
type Archetype string
