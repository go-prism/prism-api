// Package v1
//
// This package defines the first iteration of the core database objects.
// Any major schema changes must require a new v(x+1) package
// to ensure that there are no disastrous migrations.
package v1
