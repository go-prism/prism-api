package i18n

const (
	ErrGeneric        = "something went wrong"
	ErrParseBody      = "failed to parse request body"
	ErrNotFound       = "not found"
	ErrInvalidRequest = "request parameters are empty or invalid"
	ErrInvalidRegex   = "unable to parse given regex"
	ErrUnauthorised   = "unauthorised"
	ErrForbidden      = "forbidden"
)
