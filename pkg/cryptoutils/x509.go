package cryptoutils

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
)

var (
	// ErrNilBlock is thrown when a valid block couldn't
	// be decoded from the given pem data.
	ErrNilBlock = errors.New("certificate PEM returned nil block")
)

// DecodeX509Cert is a utility function to
// read an x509 certificate from a given string
func DecodeX509Cert(data string) (*x509.Certificate, error) {
	block, _ := pem.Decode([]byte(data))
	if block == nil {
		return nil, ErrNilBlock
	}
	return x509.ParseCertificate(block.Bytes)
}
