package cryptoutils_test

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/cryptoutils"
	"testing"
)

//go:embed testdata/cert.crt
var certPEM string

func TestDecodeX509Cert(t *testing.T) {
	cert, err := cryptoutils.DecodeX509Cert(certPEM)
	assert.NoError(t, err)
	assert.NotNil(t, cert)
	assert.Equal(t, "CN=mail.google.com,O=Google Inc,L=Mountain View,ST=California,C=US", cert.Subject.String())

	_, err = cryptoutils.DecodeX509Cert("arstarstarstarst")
	assert.ErrorIs(t, err, cryptoutils.ErrNilBlock)
}
