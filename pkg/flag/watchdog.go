package flag

import (
	"github.com/Unleash/unleash-client-go/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/jobs"
)

type Config struct {
	Name  string
	URL   string
	Env   string
	IID   string
	Debug bool
}

// Watchdog runs in the background
// and updates certain global values
// if feature flags change.
//
// Values
//
// ---
//
// * Log level
type Watchdog struct {
	jobs.BaseRunner
}

// NewWatchdog creates a new instance of Watchdog
// and bootstraps the Unleash client
func NewWatchdog(c *Config) (*Watchdog, error) {
	w := new(Watchdog)

	log.Infof("initialising unleash client with url: %s", c.URL)
	var opts = []unleash.ConfigOption{
		unleash.WithAppName(c.Name),
		unleash.WithUrl(c.URL),
		unleash.WithEnvironment(c.Env),
		unleash.WithInstanceId(c.IID),
	}
	// enable debug logging if requested
	if c.Debug {
		opts = append(opts, unleash.WithListener(&unleash.DebugListener{}))
	}
	if err := unleash.Initialize(opts...); err != nil {
		log.WithError(err).Error("failed to setup unleash client")
		return nil, err
	}

	return w, nil
}

func (*Watchdog) GetInterval() uint64 {
	return 5
}

// Do watches for certain flags to change.
//
// Must be started as a goroutine.
func (*Watchdog) Do() {
	log.Debug("checking feature flags for changes...")
	enabled := unleash.IsEnabled("prism_log_debug")
	level := log.GetLevel()
	if enabled && level != log.DebugLevel {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabling debug logging")
	} else if !enabled && level != log.InfoLevel {
		log.SetLevel(log.InfoLevel)
		log.Info("disabling debug logging")
	}
}
