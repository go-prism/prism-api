package flag_test

import (
	"gitlab.com/go-prism/prism-api/pkg/flag"
	"gitlab.com/go-prism/prism-api/pkg/jobs"
)

// interface guard
var _ jobs.BaseRunner = &flag.Watchdog{}
