package metrics_test

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/metrics"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInit(t *testing.T) {
	router := mux.NewRouter()
	router.Handle("/", http.NotFoundHandler())
	metrics.Init(router, "/metrics")
	ts := httptest.NewServer(router)
	defer ts.Close()

	var cases = []struct {
		path string
		code int
	}{
		{"/", http.StatusNotFound},
		{"/metrics", http.StatusOK},
	}

	for _, tt := range cases {
		t.Run(tt.path, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", ts.URL, tt.path), nil)
			assert.NoError(t, err)
			resp, err := ts.Client().Do(req)
			assert.NoError(t, err)
			defer resp.Body.Close()
			assert.Equal(t, tt.code, resp.StatusCode)
		})
	}
}
