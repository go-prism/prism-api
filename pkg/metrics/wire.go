package metrics

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	metrics "github.com/slok/go-http-metrics/metrics/prometheus"
	"github.com/slok/go-http-metrics/middleware"
	"github.com/slok/go-http-metrics/middleware/std"
)

// Init performs some of the charlie-work for
// configuring Prometheus HTTP metrics.
//
// It primarily exists to reduce the bloat going
// into main.go
func Init(r *mux.Router, path string) {
	r.Use(std.HandlerProvider("", middleware.New(middleware.Config{Recorder: metrics.NewRecorder(metrics.Config{})})))
	// enable Prometheus metrics
	r.Handle(path, promhttp.Handler())
}
