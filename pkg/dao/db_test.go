package dao

import (
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"testing"
)

func TestNewAccessLayer(t *testing.T) {
	al, err := NewAccessLayer("host=test user=postgres")
	assert.Error(t, err)
	assert.Nil(t, al)
}

func TestAccessLayer_WithRepo(t *testing.T) {
	al := &AccessLayer{
		db: &gorm.DB{},
	}
	repo := &Repository{}
	assert.Nil(t, repo.DB)
	al.WithRepo(repo)
	assert.NotNil(t, repo.DB)
}
