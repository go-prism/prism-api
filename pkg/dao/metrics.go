package dao

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	dbConnectErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_db_connect_errs",
		Help: "The total number of database connection errors",
	})
	dbTransactionSlow = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_db_transaction_slow",
		Help: "The total number of slow database transactions",
	})
	dbTransactionError = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_db_transaction_error",
		Help: "The total number of database transaction errors",
	})
)
