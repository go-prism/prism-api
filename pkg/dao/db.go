package dao

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// AccessLayer provides access to a
// database connection.
type AccessLayer struct {
	db *gorm.DB
}

type Repository struct {
	DB *gorm.DB
}

type Initializer func(db *gorm.DB)

// NewAccessLayer creates a new instance of AccessLayer
// and establishes a connection to the database described
// by the given DSN.
func NewAccessLayer(dsn string) (*AccessLayer, error) {
	al := new(AccessLayer)
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: NewGormLogger(),
	})
	if err != nil {
		log.WithError(err).Error("failed to open database connection")
		dbConnectErr.Inc()
		return nil, err
	}
	log.WithField("name", database.Name()).Info("established database connection")
	al.db = database

	return al, nil
}

// Init runs on-start logic (e.g. schema migrations).
func (al *AccessLayer) Init() error {
	log.WithField("name", al.db.Name()).Info("running auto-migration of database schema")
	return al.db.AutoMigrate(
		&v1.RemoteORM{},
		&v1.RefractionORM{},
		&v1.CacheEntryORM{},
		&v1.CacheSliceORM{},
		&v1.RemoteStatusORM{},
		&v1.RoleBindingORM{},
		&v1.ReactorStatusORM{},
		&archv1.ModuleVersionORM{},
		&archv1.HostRewriteORM{},
	)
}

// OnStart calls a series of initializers
//
// This must be called AFTER Init
func (al *AccessLayer) OnStart(init ...Initializer) {
	for _, i := range init {
		i(al.db)
	}
}

// WithRepo attaches the current database instance
// to a given Repository.
func (al *AccessLayer) WithRepo(repo *Repository) {
	repo.DB = al.db
}
