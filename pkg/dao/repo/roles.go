package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
)

type RolesRepo interface {
	FindAll(ctx context.Context, offset, size int) (*v1.RoleBindings, error)
	FindAllByUser(ctx context.Context, user string) ([]string, error)
	FindBindingsForUser(ctx context.Context, user string, offset, size int) (*v1.RoleBindings, error)
	Save(ctx context.Context, i *v1.RoleBinding) (*v1.RoleBinding, error)
}

type RolesRepoORM struct {
	dao.Repository
}

func (r *RolesRepoORM) FindAll(ctx context.Context, offset, size int) (*v1.RoleBindings, error) {
	var results []v1.RoleBindingORM
	if err := r.DB.Offset(offset).Limit(size).Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.RoleBindingORM")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindAllByUser retrieves the roles bound to
// the given user
func (r *RolesRepoORM) FindAllByUser(ctx context.Context, user string) ([]string, error) {
	var results []string
	if err := r.DB.Model(&v1.RoleBindingORM{}).Select("name").Where("username = ?", user).Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.RoleBindingORM")
		return nil, err
	}
	return results, nil
}

// FindBindingsForUser retrieves the roles bound to
// the given user
func (r *RolesRepoORM) FindBindingsForUser(ctx context.Context, user string, offset, size int) (*v1.RoleBindings, error) {
	var results []v1.RoleBindingORM
	if err := r.DB.Offset(offset).Limit(size).Where("username = ?", user).Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.RoleBindingORM")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// Save creates or updates a given v1.Role
func (r *RolesRepoORM) Save(ctx context.Context, i *v1.RoleBinding) (*v1.RoleBinding, error) {
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.RoleBinding to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.RoleBinding")
		return nil, err
	}
	temp, err := entity.ToPB(ctx)
	return &temp, err
}

func (r *RolesRepoORM) convertToPB(ctx context.Context, ormResults []v1.RoleBindingORM) (*v1.RoleBindings, error) {
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.RoleBinding{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.RoleBindings{Roles: results}, nil
}
