package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"go.opentelemetry.io/otel"
	"time"
)

type ReactorStatusRepo struct {
	dao.Repository
}

// FindAll finds all unique status for the last 30 minutes
func (r *ReactorStatusRepo) FindAll(ctx context.Context) (*v1.ReactorStatuses, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_reactor_status_findAll")
	defer span.End()
	since := time.Now().Add(time.Minute * -30).Unix()
	var results []v1.ReactorStatusORM
	if err := r.DB.Select("DISTINCT ON (name) name, config_hash, control_rods, id, time").Where("time > ?", since).Order("name, time desc").Find(&results).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to get v1.ReactorStatus")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// Save creates or updates a given v1.ReactorStatus
func (r *ReactorStatusRepo) Save(ctx context.Context, i *v1.ReactorStatus) (*v1.ReactorStatus, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_reactor_status_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.ReactorStatus to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to save v1.ReactorStatus")
		return nil, err
	}
	return i, nil
}

func (r *ReactorStatusRepo) convertToPB(ctx context.Context, ormResults []v1.ReactorStatusORM) (*v1.ReactorStatuses, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_reactor_status_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.ReactorStatus{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.ReactorStatuses{Statuses: results}, nil
}
