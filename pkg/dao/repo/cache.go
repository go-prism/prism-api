package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"go.opentelemetry.io/otel"
)

type CacheEntryRepo struct {
	dao.Repository
}

func (r *CacheEntryRepo) FindByURI(ctx context.Context, uri string) (*v1.CacheEntry, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_findByURI")
	defer span.End()
	var result v1.CacheEntryORM
	if err := r.DB.Where("uri = ?", uri).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to retrieve v1.CacheEntry")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

// FindAllByRefractID retrieves all v1.CacheEntry's that are
// owned by a given refraction.
func (r *CacheEntryRepo) FindAllByRefractID(ctx context.Context, refractID string) (*v1.CacheEntries, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_findAllByRefractID")
	defer span.End()
	var ormResults []v1.CacheEntryORM
	if err := r.DB.Where("refract_id = ?", refractID).Find(&ormResults).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.CacheEntry's")
		return nil, err
	}
	return r.convertToPB(ctx, ormResults)
}

// FindAllByRemoteID retrieves all v1.CacheEntry's that are
// owned by a given remote.
func (r *CacheEntryRepo) FindAllByRemoteID(ctx context.Context, remoteID string) (*v1.CacheEntries, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_findAllByRemoteID")
	defer span.End()
	var ormResults []v1.CacheEntryORM
	if err := r.DB.Where("remote_id = ?", remoteID).Find(&ormResults).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.CacheEntry's")
		return nil, err
	}
	return r.convertToPB(ctx, ormResults)
}

// Save creates or updates a given v1.CacheEntry
func (r *CacheEntryRepo) Save(ctx context.Context, i *v1.CacheEntry) (*v1.CacheEntry, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.CacheEntry to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.CacheEntry")
		return nil, err
	}
	temp, err := entity.ToPB(ctx)
	return &temp, err
}

// DeleteByURI deletes all v1.CacheEntry matching a given uri
func (r *CacheEntryRepo) DeleteByURI(ctx context.Context, uri string) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_deleteByURI")
	defer span.End()
	return r.DB.Where("uri = ?", uri).Delete(&v1.CacheEntryORM{}).Error
}

func (r *CacheEntryRepo) convertToPB(ctx context.Context, ormResults []v1.CacheEntryORM) (*v1.CacheEntries, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_entry_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.CacheEntry{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.CacheEntries{Entries: results}, nil
}
