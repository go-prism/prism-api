package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"go.opentelemetry.io/otel"
)

type RefractRepo struct {
	dao.Repository
}

// FindAll retrieves all v1.Refraction's
func (r *RefractRepo) FindAll(ctx context.Context) (*v1.Refractions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_findAll")
	defer span.End()
	var ormResults []v1.RefractionORM
	if err := r.DB.Find(&ormResults).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.Refraction's")
		return nil, err
	}
	return r.convertToPB(ctx, ormResults)
}

// FindAllByRemote retrieves all v1.Refraction's that contain a given remote ID
func (r *RefractRepo) FindAllByRemote(ctx context.Context, remote string) (*v1.Refractions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_findAllByRemote")
	defer span.End()
	var ormResults []v1.RefractionORM
	if err := r.DB.Where("? = ANY(remotes)", remote).Find(&ormResults).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.Refraction's")
		return nil, err
	}
	return r.convertToPB(ctx, ormResults)
}

// FindByID retrieves a v1.Refraction by ID
func (r *RefractRepo) FindByID(ctx context.Context, id string) (*v1.Refraction, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_findByID")
	defer span.End()
	var result v1.RefractionORM
	if err := r.DB.Where("id = ?", id).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.Refraction")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

// FindByName retrieves a v1.Refraction by name
func (r *RefractRepo) FindByName(ctx context.Context, name string) (*v1.Refraction, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_findByName")
	defer span.End()
	var result v1.RefractionORM
	if err := r.DB.Where("LOWER(name) = LOWER(?)", name).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.Refraction")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

func (r *RefractRepo) PatchByID(ctx context.Context, id string, i *v1.Refraction) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_patchByID")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.Refraction to ORM object")
		return err
	}
	if err := r.DB.Where("id = ?", id).Updates(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to patch v1.Refraction")
		return err
	}
	return nil
}

// Save creates or updates a given v1.Refraction
func (r *RefractRepo) Save(ctx context.Context, i *v1.Refraction) (*v1.Refraction, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.Refraction to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.Refraction")
		return nil, err
	}
	return i, nil
}

// DeleteByID deletes all v1.Refraction's with a given ID.
// It will delete at most 1, since the ID field is a unique primary key.
func (r *RefractRepo) DeleteByID(ctx context.Context, id string) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_deleteByID")
	defer span.End()
	if err := r.DB.Where("id = ?", id).Delete(&v1.RefractionORM{}).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to delete v1.Refraction")
		return err
	}
	return nil
}

func (r *RefractRepo) convertToPB(ctx context.Context, ormResults []v1.RefractionORM) (*v1.Refractions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_refract_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.Refraction{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.Refractions{Refractions: results}, nil
}
