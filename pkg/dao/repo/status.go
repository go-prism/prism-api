package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
)

type StatusRepo struct {
	dao.Repository
}

// FindAll retrieves all v1.RemoteStatuses
func (r *StatusRepo) FindAll(ctx context.Context) (*v1.RemoteStatuses, error) {
	var results []v1.RemoteStatusORM
	if err := r.DB.Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.RemoteStatuses")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindByRemoteID retrieves a v1.RemoteStatus by RemoteID
func (r *StatusRepo) FindByRemoteID(ctx context.Context, remoteID string) (*v1.RemoteStatus, error) {
	var result v1.RemoteStatusORM
	if err := r.DB.Where("remote_id = ?", remoteID).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.RemoteStatus")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

func (r *StatusRepo) PatchByID(ctx context.Context, id string, i *v1.RemoteStatus) error {
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.RemoteStatus to ORM object")
		return err
	}
	if err := r.DB.Where("id = ?", id).Updates(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to patch v1.RemoteStatus")
		return err
	}
	return nil
}

// Save creates or updates a given v1.RemoteStatus
func (r *StatusRepo) Save(ctx context.Context, i *v1.RemoteStatus) (*v1.RemoteStatus, error) {
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.RemoteStatus to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.RemoteStatus")
		return nil, err
	}
	return i, nil
}

// DeleteByID deletes all v1.RemoteStatuses with a given ID.
// It will delete at most 1, since the ID field is a unique primary key.
func (r *StatusRepo) DeleteByID(ctx context.Context, id string) error {
	if err := r.DB.Where("id = ?", id).Delete(&v1.RemoteStatusORM{}).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to delete v1.RemoteStatus")
		return err
	}
	return nil
}

func (r *StatusRepo) convertToPB(ctx context.Context, ormResults []v1.RemoteStatusORM) (*v1.RemoteStatuses, error) {
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.RemoteStatus{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.RemoteStatuses{Status: results}, nil
}
