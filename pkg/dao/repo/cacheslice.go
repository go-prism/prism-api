package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"go.opentelemetry.io/otel"
)

type CacheSliceRepo struct {
	dao.Repository
}

func (r *CacheSliceRepo) FindByEntryIDAndHash(ctx context.Context, entryID string, hash string) (*v1.CacheSlice, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_slice_findByEntryIDAndHash")
	defer span.End()
	var result v1.CacheSliceORM
	if err := r.DB.Where("entry_id = ? AND hash = ?", entryID, hash).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to retrieve v1.CacheSlice")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

// Save creates or updates a given v1.CacheSlice
func (r *CacheSliceRepo) Save(ctx context.Context, i *v1.CacheSlice) (*v1.CacheSlice, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_slice_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.CacheSlice to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.CacheSlice")
		return nil, err
	}
	temp, err := entity.ToPB(ctx)
	return &temp, err
}

// DeleteByURI deletes all v1.CacheSlice matching a given uri
func (r *CacheSliceRepo) DeleteByURI(ctx context.Context, uri string) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_slice_deleteByURI")
	defer span.End()
	return r.DB.Where("uri = ?", uri).Delete(&v1.CacheSliceORM{}).Error
}

func (r *CacheSliceRepo) convertToPB(ctx context.Context, ormResults []v1.CacheSliceORM) (*v1.CacheSlices, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_cache_slice_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.CacheSlice{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.CacheSlices{Slices: results}, nil
}
