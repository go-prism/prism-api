package repo

// Repos contains all the available repositories
// in an easy-to-access format.
//
// All fields must be available and must never be nil
type Repos struct {
	RemoteRepo        *RemoteRepo
	RefractRepo       *RefractRepo
	CacheEntryRepo    *CacheEntryRepo
	CacheSliceRepo    *CacheSliceRepo
	StatusRepo        *StatusRepo
	RolesRepo         RolesRepo
	ModuleVersionRepo ModuleVersionRepo
	HostRuleRepo      HostRuleRepo
	ReactorStatusRepo *ReactorStatusRepo
}
