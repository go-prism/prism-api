package repo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gorm.io/gorm"
)

type RemoteRepo struct {
	dao.Repository
}

// FindAll retrieves all v1.Remote's
func (r *RemoteRepo) FindAll(ctx context.Context) (*v1.Remotes, error) {
	var results []v1.RemoteORM
	if err := r.DB.Preload("ClientProfile").Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.Remote's")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

func (r *RemoteRepo) FindAllIDs(ctx context.Context) ([]string, error) {
	var results []string
	if err := r.DB.Model(&v1.RemoteORM{}).Select("id").Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to fetch v1.Remote IDs")
		return nil, err
	}
	return results, nil
}

// FindAllByArchetype retrieves all v1.Remote's by the given
// archetype
func (r *RemoteRepo) FindAllByArchetype(ctx context.Context, arch string) (*v1.Remotes, error) {
	var results []v1.RemoteORM
	if err := r.DB.Preload("ClientProfile").Where("archetype = ?", arch).Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to list v1.Remote's")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindAllByIDs finds all v1.Remote that have an id in the pool
// of given IDs.
func (r *RemoteRepo) FindAllByIDs(ctx context.Context, ids []string) (*v1.Remotes, error) {
	var results []v1.RemoteORM
	if err := r.DB.Preload("ClientProfile").Where("id IN ?", ids).Find(&results).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.Remote")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindByID retrieves a v1.Remote by ID
func (r *RemoteRepo) FindByID(ctx context.Context, id string) (*v1.Remote, error) {
	var result v1.RemoteORM
	if err := r.DB.Preload("ClientProfile").Where("id = ?", id).First(&result).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to get v1.Remote")
		return nil, err
	}
	res, err := result.ToPB(ctx)
	return &res, err
}

func (r *RemoteRepo) PatchByID(ctx context.Context, id string, i *v1.Remote) error {
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.Remote to ORM object")
		return err
	}
	if err := r.DB.Where("id = ?", id).Updates(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to patch v1.Remote")
		return err
	}
	return nil
}

// Save creates or updates a given v1.Remote
func (r *RemoteRepo) Save(ctx context.Context, i *v1.Remote) (*v1.Remote, error) {
	entity, err := i.ToORM(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to convert v1.Remote to ORM object")
		return nil, err
	}
	if err := r.DB.Session(&gorm.Session{FullSaveAssociations: true}).Save(&entity).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to save v1.Remote")
		return nil, err
	}
	return i, nil
}

// DeleteByID deletes all v1.Remote's with a given ID.
// It will delete at most 1, since the ID field is a unique primary key.
func (r *RemoteRepo) DeleteByID(ctx context.Context, id string) error {
	if err := r.DB.Where("id = ?", id).Delete(&v1.RemoteORM{}).Error; err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to delete v1.Remote")
		return err
	}
	return nil
}

func (r *RemoteRepo) convertToPB(ctx context.Context, ormResults []v1.RemoteORM) (*v1.Remotes, error) {
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*v1.Remote{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			return nil, err
		}
		results = append(results, &temp)
	}
	return &v1.Remotes{Remotes: results}, nil
}
