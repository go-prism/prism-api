package repo

import (
	"gitlab.com/go-prism/prism-api/pkg/dao"
)

// Init performs some of the charlie-work for
// wiring repositories to the database dao.AccessLayer.
//
// It primarily exists to reduce the bloat going
// into main.go
func Init(accessLayer *dao.AccessLayer) *Repos {
	remoteRepo := &RemoteRepo{}
	refractRepo := &RefractRepo{}
	cacheEntryRepo := &CacheEntryRepo{}
	cacheSliceRepo := &CacheSliceRepo{}
	statusRepo := &StatusRepo{}
	rolesRepo := &RolesRepoORM{}
	moduleVersionRepo := &ModuleVersionRepoORM{}
	hostRewriteRepo := &HostRuleRepoORM{}
	reactorStatusRepo := &ReactorStatusRepo{}

	accessLayer.WithRepo(&remoteRepo.Repository)
	accessLayer.WithRepo(&refractRepo.Repository)
	accessLayer.WithRepo(&cacheEntryRepo.Repository)
	accessLayer.WithRepo(&cacheSliceRepo.Repository)
	accessLayer.WithRepo(&statusRepo.Repository)
	accessLayer.WithRepo(&rolesRepo.Repository)
	accessLayer.WithRepo(&moduleVersionRepo.Repository)
	accessLayer.WithRepo(&hostRewriteRepo.Repository)
	accessLayer.WithRepo(&reactorStatusRepo.Repository)
	return &Repos{
		RemoteRepo:        remoteRepo,
		RefractRepo:       refractRepo,
		CacheEntryRepo:    cacheEntryRepo,
		CacheSliceRepo:    cacheSliceRepo,
		StatusRepo:        statusRepo,
		RolesRepo:         rolesRepo,
		ModuleVersionRepo: moduleVersionRepo,
		HostRuleRepo:      hostRewriteRepo,
		ReactorStatusRepo: reactorStatusRepo,
	}
}
