package repo

import (
	"context"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"

	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
)

type ModuleVersionRepo interface {
	FindAllByModule(ctx context.Context, moduleName string) (*archv1.ModuleVersions, error)
	FindByModuleVersion(ctx context.Context, moduleName, versionName string) (*archv1.ModuleVersion, error)
	Save(ctx context.Context, i *archv1.ModuleVersion) (*archv1.ModuleVersion, error)
}

type ModuleVersionRepoORM struct {
	dao.Repository
	ModuleVersionRepo
}

// FindAllByModule finds all versions that
// match a given module
func (r *ModuleVersionRepoORM) FindAllByModule(ctx context.Context, moduleName string) (*archv1.ModuleVersions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_mod_findAllByModule", trace.WithAttributes(attribute.String("name", moduleName)))
	defer span.End()
	var results []archv1.ModuleVersionORM
	if err := r.DB.Model(&archv1.ModuleVersionORM{}).Where("module_name = ?", moduleName).Find(&results).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to get archv1.ModuleVersionORM")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindByModuleVersion finds a module by name
// and version.
func (r *ModuleVersionRepoORM) FindByModuleVersion(ctx context.Context, moduleName, versionName string) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_mod_findByModuleVersion", trace.WithAttributes(
		attribute.String("name", moduleName),
		attribute.String("version", versionName),
	))
	defer span.End()
	var result archv1.ModuleVersionORM
	if err := r.DB.Where("module_name = ? AND version_name = ?", moduleName, versionName).First(&result).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to count archv1.ModuleVersionORM")
		return nil, err
	}
	p, err := result.ToPB(ctx)
	return &p, err
}

// Save creates or updates a given archv1.ModuleVersion
func (r *ModuleVersionRepoORM) Save(ctx context.Context, i *archv1.ModuleVersion) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_mod_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to convert archv1.ModuleVersion to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to save archv1.ModuleVersion")
		return nil, err
	}
	temp, err := entity.ToPB(ctx)
	return &temp, err
}

func (r *ModuleVersionRepoORM) convertToPB(ctx context.Context, ormResults []archv1.ModuleVersionORM) (*archv1.ModuleVersions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_mod_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*archv1.ModuleVersion{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &archv1.ModuleVersions{Versions: results}, nil
}
