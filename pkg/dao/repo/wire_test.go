package repo_test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
)

func TestInit(t *testing.T) {
	repos := repo.Init(&dao.AccessLayer{})

	rv := reflect.ValueOf(repos)
	rv = rv.Elem()

	// iterate all fields and assert that none
	// of them are nil
	for i := 0; i < rv.NumField(); i++ {
		assert.False(t, rv.Field(i).IsNil())
	}
}
