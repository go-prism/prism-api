package repo

import (
	"context"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"

	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
)

type HostRuleRepo interface {
	FindAll(ctx context.Context) (*archv1.HostRewrites, error)
	FindBySource(ctx context.Context, source string) (*archv1.HostRewrite, error)
	Save(ctx context.Context, i *archv1.HostRewrite) (*archv1.HostRewrite, error)
}

type HostRuleRepoORM struct {
	dao.Repository
	HostRuleRepo
}

// FindAll retrieves all stored rules.
func (r *HostRuleRepoORM) FindAll(ctx context.Context) (*archv1.HostRewrites, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_hostrule_findAll")
	defer span.End()
	var results []archv1.HostRewriteORM
	if err := r.DB.Find(&results).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to list archv1.HostRewriteORM")
		return nil, err
	}
	return r.convertToPB(ctx, results)
}

// FindBySource finds a matching rule
// for a given source.
//
// The source is expected to be a domain
// e.g. github.com
func (r *HostRuleRepoORM) FindBySource(ctx context.Context, source string) (*archv1.HostRewrite, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_hostrule_findBySource")
	defer span.End()
	var result archv1.HostRewriteORM
	if err := r.DB.Where("source = ?", source).First(&result).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to get archv1.HostRewriteORM")
		return nil, err
	}
	p, err := result.ToPB(ctx)
	return &p, err
}

// Save creates or updates a given archv1.HostRewrite
func (r *HostRuleRepoORM) Save(ctx context.Context, i *archv1.HostRewrite) (*archv1.HostRewrite, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_hostrule_save")
	defer span.End()
	entity, err := i.ToORM(ctx)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to convert archv1.HostRewrite to ORM object")
		return nil, err
	}
	if err := r.DB.Save(&entity).Error; err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to save archv1.HostRewrite")
		return nil, err
	}
	temp, err := entity.ToPB(ctx)
	return &temp, err
}

func (r *HostRuleRepoORM) convertToPB(ctx context.Context, ormResults []archv1.HostRewriteORM) (*archv1.HostRewrites, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "repo_hostrule_convertToPB")
	defer span.End()
	// convert the results back from ORM -> PB
	//goland:noinspection GoPreferNilSlice
	results := []*archv1.HostRewrite{}
	for _, or := range ormResults {
		temp, err := or.ToPB(ctx)
		if err != nil {
			span.RecordError(err)
			return nil, err
		}
		results = append(results, &temp)
	}
	return &archv1.HostRewrites{Rules: results}, nil
}
