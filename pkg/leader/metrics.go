package leader

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricElected = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "leader_elected",
	})
	metricDemoted = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "leader_demoted",
	})
)
