package leader

type Candidate interface {
	GetLock(leader Leader)
}

// Leader is an abstraction for work that
// should be started and stopped based on whether
// we're the currently elected node
type Leader interface {
	// OnElect is called when we win an election.
	OnElect()
	// OnDemote is called when an elected leader
	// loses an election and should stop doing work.
	OnDemote()
}
