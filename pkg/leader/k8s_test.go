package leader

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/rest"
)

// interface guard
var _ Candidate = &K8SCandidate{}

var testLoader ClientLoader = func() (kubernetes.Interface, error) {
	return fake.NewSimpleClientset(), nil
}

var errTestLoader ClientLoader = func() (kubernetes.Interface, error) {
	return nil, rest.ErrNotInCluster
}

func TestNewK8SCandidate(t *testing.T) {
	var cases = []struct {
		name   string
		loader ClientLoader
		err    error
	}{
		{
			"client is initialised properly",
			testLoader,
			nil,
		},
		{
			"out-of-cluster err thrown",
			errTestLoader,
			rest.ErrNotInCluster,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			_, err := NewK8SCandidate(tt.loader, "", "")
			assert.ErrorIs(t, err, tt.err)
		})
	}
}

type testLeader struct {
	Leader
	elected bool
	demoted bool
}

func (l *testLeader) OnElect() {
	l.elected = true
}
func (l *testLeader) OnDemote() {
	l.demoted = true
}

func TestK8SCandidate_GetLock(t *testing.T) {
	can, err := NewK8SCandidate(testLoader, "", "")
	assert.NoError(t, err)

	l := &testLeader{}

	go can.GetLock(l)
	time.Sleep(time.Second)

	assert.True(t, l.elected)
	assert.False(t, l.demoted)
}
