package leader

import (
	"context"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/leaderelection"
	"k8s.io/client-go/tools/leaderelection/resourcelock"
)

var (
	// InClusterLoader is the default ClientLoader that
	// uses the Kubernetes InClusterConfig.
	//
	// This loader is expected to be used in production.
	InClusterLoader ClientLoader = func() (kubernetes.Interface, error) {
		config, err := rest.InClusterConfig()
		if err != nil {
			return nil, err
		}
		return kubernetes.NewForConfig(config)
	}
)

// ClientLoader provides an abstraction for authenticating
// with Kubernetes.
//
// The default implementation is the InClusterLoader
type ClientLoader func() (kubernetes.Interface, error)

// K8SCandidate uses the Kubernetes Coordination API
// to manage leader election.
//
// This candidate uses the container HOSTNAME as
// its node ID.
type K8SCandidate struct {
	Candidate
	clientset kubernetes.Interface
	nodeID    string
	name      string
	namespace string
}

// NewK8SCandidate creates a new instance of K8SCandidate
// and attempts to authenticate to the Kubernetes API.
func NewK8SCandidate(loader ClientLoader, name, namespace string) (*K8SCandidate, error) {
	client, err := loader()
	if err != nil {
		return nil, err
	}
	// try to get our unique node ID
	hn, err := os.Hostname()
	if err != nil {
		return nil, err
	}

	// setup the struct
	c := new(K8SCandidate)
	c.clientset = client
	c.nodeID = hn
	c.name = name
	c.namespace = namespace

	return c, nil
}

func (c *K8SCandidate) GetLock(leader Leader) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	lock := &resourcelock.LeaseLock{
		LeaseMeta: metav1.ObjectMeta{
			Name:      c.name,
			Namespace: c.namespace,
		},
		Client: c.clientset.CoordinationV1(),
		LockConfig: resourcelock.ResourceLockConfig{
			Identity: c.nodeID,
		},
	}
	leaderelection.RunOrDie(ctx, leaderelection.LeaderElectionConfig{
		Lock:            lock,
		ReleaseOnCancel: true,
		LeaseDuration:   15 * time.Second,
		RenewDeadline:   10 * time.Second,
		RetryPeriod:     2 * time.Second,
		Callbacks: leaderelection.LeaderCallbacks{
			OnStartedLeading: func(ctx context.Context) {
				log.Info("we are the leader")
				metricElected.Inc()
				leader.OnElect()
			},
			OnStoppedLeading: func() {
				log.Info("we are no longer the leader")
				metricDemoted.Inc()
				leader.OnDemote()
			},
			OnNewLeader: func(identity string) {
				if identity == c.nodeID {
					return
				}
				log.Infof("following new leader: %s", identity)
			},
		},
	})
}
