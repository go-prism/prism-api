package logging

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/internal/tests"
	"testing"
)

// interface guard
var hook log.Hook = &ContextualHook{}

func TestContextualHook_Levels(t *testing.T) {
	assert.ElementsMatch(t, log.AllLevels, hook.Levels())
}

func TestContextualHook_Fire(t *testing.T) {
	var cases = []struct {
		name  string
		entry *log.Entry
		v     func(t *testing.T, e *log.Entry, err error)
	}{
		{
			"missing user is set to null",
			&log.Entry{
				Data: map[string]interface{}{},
			},
			func(t *testing.T, e *log.Entry, err error) {
				assert.NoError(t, err)
				assert.EqualValues(t, "null", e.Data[DataKeyUser])
			},
		},
		{
			"context user is set correctly",
			&log.Entry{
				Data:    map[string]interface{}{},
				Context: tests.TestingUserCtx,
			},
			func(t *testing.T, e *log.Entry, err error) {
				assert.NoError(t, err)
				assert.EqualValues(t, tests.TestingUsername, e.Data[DataKeyUser])
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			err := hook.Fire(tt.entry)
			tt.v(t, tt.entry, err)
		})
	}
}
