package logging

import (
	log "github.com/sirupsen/logrus"
	stdlog "log"
)

// Config defines the available logging options
// that can be configured from the environment.
type Config struct {
	Debug        bool
	FormatJSON   bool `split_words:"true"`
	ReportCaller bool `split_words:"true"`
}

// Init performs some of the charlie-work for
// configuring Logrus via the environment
//
// It primarily exists to reduce the bloat going
// into main.go
func Init(c *Config) {
	if c.FormatJSON {
		log.SetFormatter(&log.JSONFormatter{})
	}
	if c.Debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabled debug logging")
	}
	log.SetReportCaller(c.ReportCaller)
	log.AddHook(&ContextualHook{})
	// hijack stdlib logging
	stdlog.SetOutput(log.New().Writer())
}
