package logging

import (
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
)

const (
	DataKeyUser = "user"
	DataKeyID   = "id"
)

// ContextualHook modifies the log entries
// to include additional information such
// as request ID and username
type ContextualHook struct{}

func (*ContextualHook) Fire(entry *log.Entry) error {
	newData := make(log.Fields, len(entry.Data)+2)
	for k, v := range entry.Data {
		newData[k] = v
	}
	if _, ok := newData[DataKeyUser]; !ok {
		newData[DataKeyUser] = "null"
	}
	if entry.Context != nil {
		user, ok := client.GetContextUser(entry.Context)
		if ok && user != nil {
			newData[DataKeyUser] = user.AsUsername()
		}
		id := tracer.GetContextId(entry.Context)
		newData[DataKeyID] = id
	}
	entry.Data = newData
	return nil
}

func (*ContextualHook) Levels() []log.Level {
	return log.AllLevels
}
