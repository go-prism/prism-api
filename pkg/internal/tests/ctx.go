package tests

import (
	"context"
	"gitlab.com/av1o/cap10/pkg/client"
)

var TestingChain = &client.ChainClaim{
	Subjects:  []string{"12345678"},
	Issuers:   []string{"https://gitlab.com"},
	Token:     "",
	TokenHash: "",
	RawClaim:  "",
	Claims:    nil,
}

var TestingUser = &client.UserClaim{
	Sub:       "12345678",
	Iss:       "https://gitlab.com",
	Token:     "",
	TokenHash: "",
	Claims:    nil,
}

var TestingUsername = TestingUser.AsUsername()

var TestingUserCtx = client.PersistUserCtx(context.TODO(), TestingChain, TestingUser)
