package refract

import (
	"net/http"

	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	httputils2 "gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
)

// API defines the CRUD endpoints
// for a v1.Refraction.
type API struct {
	svc   *Service
	repos *repo.Repos
	rc    reactor.ReactorClient
}

// NewAPI creates a new instance of API
func NewAPI(repos *repo.Repos, rc reactor.ReactorClient, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	a := new(API)
	a.repos = repos
	a.svc = NewService(repos, rc)
	a.rc = rc

	router.HandleFunc("/api/v1/refract", c.WithUserFunc(a.List)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/refract", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Create, auth.RoleSuperUser))).
		Methods(http.MethodPost)
	router.HandleFunc("/api/v1/refract/{id}", c.WithUserFunc(a.Get)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/refract/{id}/-/info", c.WithUserFunc(a.GetInfo)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/refract/{id}", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Patch, auth.RoleSuperUser))).
		Methods(http.MethodPatch)
	router.HandleFunc("/api/v1/refract/{id}", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Delete, auth.RoleSuperUser))).
		Methods(http.MethodDelete)

	return a
}

// List godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary get all visible Refractions
// @Produce json
// @Success 200 {object} v1.Refractions
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract [get]
func (a *API) List(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("listing v1.Refractions")
	refracts, err := a.svc.List(r.Context())
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, refracts)
}

// Get godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary get a Refraction
// @Param id path string true "Refraction to retrieve"
// @Success 200 {object} v1.Refraction
// @Failure 400 {string} string "bad request"
// @Failure 404 {string} string "not found"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract/{id} [get]
func (a *API) Get(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("retrieving v1.Refraction")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(r.Context()).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	refract, err := a.svc.Get(r.Context(), objID.String())
	if ok := httputils2.ReturnGormErr(w, err); !ok {
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, refract)
}

// GetInfo godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary get information about a Refraction
// @Param id path string true "Refraction to retrieve"
// @Success 200 {object} reactor.GetRefractInfoResponse
// @Failure 400 {string} string "bad request"
// @Failure 404 {string} string "not found"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract/{id}/-/info [get]
func (a *API) GetInfo(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("retrieving v1.Refraction info")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(r.Context()).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	// lookup the refraction and return relevant errors if we cant
	refract, err := a.repos.RefractRepo.FindByID(r.Context(), objID.String())
	if ok := httputils2.ReturnGormErr(w, err); !ok {
		return
	}
	// ask the nearest reactor to fetch bucket info for this refraction
	// todo limit who can do this and how often
	info, err := a.rc.GetRefractInfo(utils.SubmitCtx(r.Context()), &reactor.GetRefractInfoRequest{
		Refraction: refract.Name,
	})
	if err != nil {
		http.Error(w, "failed to retrieve info", http.StatusInternalServerError)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, info)
}

// Create godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary create a Refraction
// @Accept json
// @Produce json
// @Param request body CreateRefractionRequest true "Request payload"
// @Success 200 {object} v1.Refraction
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract [post]
func (a *API) Create(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("creating v1.Refraction")
	var req api.CreateRefractRequest
	if err := httputils.WithProtoBody(r, &req); err != nil {
		log.WithError(err).WithContext(r.Context()).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	refract, err := a.svc.Create(r.Context(), &req)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, refract)
}

// Patch godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary patch a Refraction
// @Accept json
// @Produce json
// @Param id path string true "Refraction to retrieve"
// @Param request body CreateRefractionRequest true "Request payload"
// @Success 204 {string} string "no content"
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract/{id} [patch]
func (a *API) Patch(w http.ResponseWriter, r *http.Request) {
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(r.Context()).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	log.WithContext(r.Context()).Info("patching v1.Refraction")
	var req api.CreateRefractRequest
	if err := httputils.WithProtoBody(r, &req); err != nil {
		log.WithError(err).WithContext(r.Context()).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	err = a.svc.Patch(r.Context(), objID.String(), &req)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// Delete godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Refraction
// @Summary delete a Refraction
// @Param id path string true "Refraction to delete"
// @Success 204 {string} string "no content"
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/refract/{id} [delete]
func (a *API) Delete(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("deleting v1.Refraction")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(r.Context()).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	if err := a.svc.Delete(r.Context(), objID.String()); err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	// we have nothing to say
	w.WriteHeader(http.StatusNoContent)
}
