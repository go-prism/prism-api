package refract

import (
	"context"
	"errors"

	"github.com/golang/protobuf/ptypes/empty"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type RPC struct {
	api.UnimplementedRefractionsServer
	svc *Service
}

func NewRPC(svc *Service) *RPC {
	r := new(RPC)
	r.svc = svc

	return r
}

func (r RPC) List(ctx context.Context, _ *empty.Empty) (*v1.Refractions, error) {
	return r.svc.List(ctx)
}

func (r RPC) Get(ctx context.Context, req *api.GetRefractRequest) (*v1.Refraction, error) {
	ref, err := r.svc.Get(ctx, req.GetId())
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return ref, err
}
func (r RPC) Create(ctx context.Context, req *api.CreateRefractRequest) (*v1.Refraction, error) {
	return r.svc.Create(ctx, req)
}
func (r RPC) Patch(ctx context.Context, req *api.PatchRefractRequest) (*empty.Empty, error) {
	err := r.svc.Patch(ctx, req.GetId(), req.GetReq())
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return &empty.Empty{}, err
}
func (r RPC) Delete(ctx context.Context, req *api.DeleteRefractRequest) (*empty.Empty, error) {
	err := r.svc.Delete(ctx, req.GetId())
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return &empty.Empty{}, err
}
