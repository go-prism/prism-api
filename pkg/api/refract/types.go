package refract

// CreateRefractionRequest defines the data
// required to create a v1.Refraction.
type CreateRefractionRequest struct {
	Name      string   `json:"name"`
	Archetype string   `json:"archetype"`
	Remotes   []string `json:"remotes"`
}
