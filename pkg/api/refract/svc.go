package refract

import (
	"context"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
)

type Service struct {
	repos *repo.Repos
	rc    reactor.ReactorClient
}

func NewService(repos *repo.Repos, rc reactor.ReactorClient) *Service {
	svc := new(Service)
	svc.repos = repos
	svc.rc = rc

	return svc
}

func (svc *Service) List(ctx context.Context) (*v1.Refractions, error) {
	return svc.repos.RefractRepo.FindAll(ctx)
}

func (svc *Service) Get(ctx context.Context, id string) (*v1.Refraction, error) {
	return svc.repos.RefractRepo.FindByID(ctx, id)
}

func (svc *Service) Create(ctx context.Context, req *api.CreateRefractRequest) (*v1.Refraction, error) {
	return svc.repos.RefractRepo.Save(ctx, &v1.Refraction{
		Id:        uuid.NewV4().String(),
		Name:      req.Name,
		Archetype: req.Archetype,
		Remotes:   req.Remotes,
	})
}

func (svc *Service) Patch(ctx context.Context, id string, req *api.CreateRefractRequest) error {
	err := svc.repos.RefractRepo.PatchByID(ctx, id, &v1.Refraction{
		Name:      req.Name,
		Archetype: req.Archetype,
		Remotes:   req.Remotes,
	})
	if err != nil {
		return err
	}
	// inform the reactor that it needs to refresh the controlRod
	if _, err := svc.rc.InvalidateControlRod(utils.SubmitCtx(ctx), &reactor.InvalidateControlRodRequest{
		Refraction: req.Name,
	}); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to invalidate controlRod, this may cause issues")
	}
	return nil
}

func (svc *Service) Delete(ctx context.Context, id string) error {
	return svc.repos.RefractRepo.DeleteByID(ctx, id)
}
