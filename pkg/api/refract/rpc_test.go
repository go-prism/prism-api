package refract

import "gitlab.com/go-prism/prism-rpc/service/api"

// interface guard
var _ api.RefractionsServer = &RPC{}
