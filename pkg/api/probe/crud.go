package probe

import (
	"net/http"

	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
)

// API defines the CRUD endpoints for
// a v1.RemoteStatus
type API struct {
	repo *repo.StatusRepo
}

func NewAPI(repos *repo.Repos, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	api := new(API)
	api.repo = repos.StatusRepo

	router.HandleFunc("/api/v1/remote/-/status", c.WithUserFunc(roleSvc.
		WithRoleFunc(api.List, auth.RoleSuperUser))).
		Methods(http.MethodGet)

	return api
}

// List godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary get all visible Remotes
// @Produce json
// @Success 200 {object} v1.Remotes
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote [get]
func (api *API) List(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("listing v1.RemoteStatus")
	items, err := api.repo.FindAll(r.Context())
	if err != nil {
		http.Error(w, i18n.ErrGeneric, http.StatusInternalServerError)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, items)
}
