package extras

import (
	"gitlab.com/av1o/cap10/pkg/proxy"
	"net/http"
)

type Plugin struct {
	h *proxy.Handler
}

func NewPlugin(uri string) (*Plugin, error) {
	p := new(Plugin)
	handler, err := proxy.NewHandler(uri, nil, nil)
	if err != nil {
		return nil, err
	}
	p.h = handler

	return p, nil
}

func (p *Plugin) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	p.h.ServeHTTP(w, r)
}
