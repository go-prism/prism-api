package remote_test

import (
	"gitlab.com/go-prism/prism-api/pkg/api/remote"
	"gitlab.com/go-prism/prism-rpc/service/api"
)

// interface guard
var _ api.RemotesServer = &remote.RPC{}
