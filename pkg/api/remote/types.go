package remote

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/cryptoutils"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"net/url"
)

var (
	// ErrCertButNoKey indicates that the user has forgotten to upload
	// the private key for the certificate.
	ErrCertButNoKey    = errors.New("certificate given but key is missing")
	ErrInvalidRegex    = errors.New("supplied regex is not valid")
	ErrInvalidRequest  = errors.New(i18n.ErrInvalidRequest)
	ErrDeleteDependent = errors.New("cannot delete remote as it is used by existing Refractions")
)

// IsValid returns whether the given api.CreateRemoteRequest
// has all the parameters required for submission.
func IsValid(r *api.CreateRemoteRequest) bool {
	if r.Name == "" {
		return false
	}
	uri, err := url.ParseRequestURI(r.GetUri())
	if err != nil {
		log.WithError(err).Error("cannot parse request URI as valid url.URL")
		return false
	}
	if uri.Scheme != "http" && uri.Scheme != "https" {
		log.Errorf("invalid remote scheme: '%s', expecting http(s)", uri.Scheme)
		return false
	}
	return true
}

// IsProfileValid returns whether the given api.UpdateProfileRequest
// contains valid certificates (if given).
func IsProfileValid(r *api.UpdateProfileRequest) error {
	if r.GetCa() != "" {
		if err := isCertValid(r.GetCa(), "CA"); err != nil {
			return err
		}
	}
	if r.Cert != "" {
		if err := isCertValid(r.Cert, "cert"); err != nil {
			return err
		}
		if r.Key == "" {
			return ErrCertButNoKey
		}
	}
	return nil
}

// isCertValid attempts to decode an x509 certificate and read
// the subject from it.
func isCertValid(data, entity string) error {
	log.Debugf("verifying given %s certificate...", entity)
	cert, err := cryptoutils.DecodeX509Cert(data)
	if err != nil {
		log.WithError(err).Errorf("failed to read given %s certificate", entity)
		return err
	}
	log.Infof("loaded %s certificate with subject: %s", entity, cert.Subject.String())
	return nil
}
