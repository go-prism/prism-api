package remote

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/api"
)

type RPC struct {
	api.UnimplementedRemotesServer
	svc *Service
}

func NewRPC(svc *Service) *RPC {
	r := new(RPC)
	r.svc = svc

	return r
}

func (r RPC) List(ctx context.Context, req *api.ListRequest) (*v1.Remotes, error) {
	if arch := req.GetArchetype(); arch != "" {
		return r.svc.ListByArchetype(ctx, arch)
	}
	return r.svc.List(ctx)
}

func (r RPC) Get(ctx context.Context, req *api.GetRemoteRequest) (*v1.Remote, error) {
	return r.svc.Get(ctx, req.GetId())
}
func (r RPC) Create(ctx context.Context, req *api.CreateRemoteRequest) (*v1.Remote, error) {
	return r.svc.Create(ctx, req)
}
func (r RPC) Patch(ctx context.Context, req *api.PatchRemoteRequest) (*empty.Empty, error) {
	return &empty.Empty{}, r.svc.Patch(ctx, req.GetId(), req.GetReq())
}
func (r RPC) Delete(ctx context.Context, req *api.DeleteRemoteRequest) (*empty.Empty, error) {
	return &empty.Empty{}, r.svc.Delete(ctx, req.GetId())
}
