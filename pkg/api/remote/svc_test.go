package remote

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestService_regexIsValid(t *testing.T) {
	var cases = []struct {
		r  string
		ok bool
	}{
		{"[]ar/st", false},
		{".*", true},
	}

	svc := &Service{}
	for _, tt := range cases {
		t.Run(tt.r, func(t *testing.T) {
			assert.Equal(t, tt.ok, svc.regexIsValid(tt.r))
		})
	}
}
