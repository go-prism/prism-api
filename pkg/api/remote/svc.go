package remote

import (
	"context"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"regexp"
)

type Service struct {
	repos *repo.Repos
}

func NewService(repos *repo.Repos) *Service {
	svc := new(Service)
	svc.repos = repos

	return svc
}

func (*Service) regexIsValid(r string) bool {
	_, err := regexp.Compile(r)
	if err != nil {
		log.WithError(err).Errorf("failed to compile given regex: '%s'", r)
		return false
	}
	return true
}

func (svc *Service) List(ctx context.Context) (*v1.Remotes, error) {
	return svc.repos.RemoteRepo.FindAll(ctx)
}

func (svc *Service) ListByArchetype(ctx context.Context, arch string) (*v1.Remotes, error) {
	return svc.repos.RemoteRepo.FindAllByArchetype(ctx, arch)
}

func (svc *Service) Get(ctx context.Context, id string) (*v1.Remote, error) {
	return svc.repos.RemoteRepo.FindByID(ctx, id)
}

func (svc *Service) Create(ctx context.Context, req *api.CreateRemoteRequest) (*v1.Remote, error) {
	// validate the regex
	checks := append(req.AllowList, req.BlockList...)
	for _, r := range checks {
		ok := svc.regexIsValid(r)
		if !ok {
			return nil, ErrInvalidRegex
		}
	}
	// setup the profile if given, otherwise
	// generate a default one
	var profile *v1.ClientProfile
	if req.ClientProfile == nil {
		profile = svc.defaultClientProfile()
	} else {
		profile = &v1.ClientProfile{
			Id:            uuid.NewV4().String(),
			SkipTLSVerify: req.ClientProfile.SkipTLS,
			TlsCert:       req.ClientProfile.Cert,
			TlsKey:        req.ClientProfile.Key,
			TlsCA:         req.ClientProfile.GetCa(),
			Ciphers:       v1.ClientProfile_INTERMEDIATE,
			HttpProxy:     req.ClientProfile.HTTPProxy,
			HttpsProxy:    req.ClientProfile.HTTPSProxy,
			NoProxy:       req.ClientProfile.NoProxy,
			TlsMinVersion: 0,
		}
	}
	return svc.repos.RemoteRepo.Save(ctx, &v1.Remote{
		Id:                uuid.NewV4().String(),
		Name:              req.Name,
		Uri:               req.GetUri(),
		Enabled:           wrapperspb.Bool(req.Enabled),
		Archetype:         req.Archetype,
		AllowList:         req.AllowList,
		BlockList:         req.BlockList,
		RestrictedHeaders: req.RestrictedHeaders,
		StripRestricted:   req.StripRestricted,
		ClientProfile:     profile,
	})
}

func (svc *Service) Patch(ctx context.Context, id string, req *api.CreateRemoteRequest) error {
	// verify that the request payload is valid
	if !IsValid(req) {
		return ErrInvalidRequest
	}
	if err := IsProfileValid(req.ClientProfile); err != nil {
		return err
	}
	remote, err := svc.repos.RemoteRepo.FindByID(ctx, id)
	if err != nil {
		return err
	}
	// update the remote
	remote.Name = req.Name
	remote.Uri = req.GetUri()
	remote.Enabled = wrapperspb.Bool(req.Enabled)
	remote.Archetype = req.Archetype
	remote.AllowList = req.AllowList
	remote.BlockList = req.BlockList
	remote.RestrictedHeaders = req.RestrictedHeaders
	remote.StripRestricted = req.StripRestricted
	// if the client profile doesn't exist, we create it
	if remote.ClientProfile == nil {
		remote.ClientProfile = svc.defaultClientProfile()
	}
	remote.ClientProfile.SkipTLSVerify = req.ClientProfile.SkipTLS
	remote.ClientProfile.TlsCA = req.ClientProfile.GetCa()
	remote.ClientProfile.TlsCert = req.ClientProfile.Cert
	remote.ClientProfile.TlsKey = req.ClientProfile.Key
	remote.ClientProfile.HttpProxy = req.ClientProfile.HTTPProxy
	remote.ClientProfile.HttpsProxy = req.ClientProfile.HTTPSProxy
	remote.ClientProfile.NoProxy = req.ClientProfile.NoProxy
	// save the changes
	_, err = svc.repos.RemoteRepo.Save(ctx, remote)
	return err
}

func (svc *Service) defaultClientProfile() *v1.ClientProfile {
	return &v1.ClientProfile{
		Id:            uuid.NewV4().String(),
		SkipTLSVerify: false,
		TlsCert:       "",
		TlsKey:        "",
		TlsCA:         "",
		Ciphers:       v1.ClientProfile_INTERMEDIATE,
		HttpProxy:     "",
		HttpsProxy:    "",
		NoProxy:       "",
		TlsMinVersion: 0,
	}
}

func (svc *Service) Delete(ctx context.Context, id string) error {
	// check that we should actually be allowed to delete this remote
	if dependants, err := svc.repos.RefractRepo.FindAllByRemote(ctx, id); err != nil {
		return err
	} else if len(dependants.Refractions) > 0 {
		log.WithContext(ctx).Warningf("cannot delete remote that is a dependent of %d refractions", len(dependants.Refractions))
		return ErrDeleteDependent
	}
	return svc.repos.RemoteRepo.DeleteByID(ctx, id)
}
