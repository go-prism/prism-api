package remote

import (
	"net/http"

	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	httputils2 "gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gitlab.com/go-prism/prism-rpc/service/api"
)

// API defines the CRUD endpoints
// for a v1.Remote.
type API struct {
	repos *repo.Repos
	svc   *Service
}

// NewAPI creates a new instance of API
func NewAPI(repos *repo.Repos, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	a := new(API)
	a.repos = repos
	a.svc = NewService(repos)

	router.HandleFunc("/api/v1/remote", c.WithUserFunc(a.List)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/remote/-/arch", c.WithUserFunc(a.ListByArchetype)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/remote", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Create, auth.RoleSuperUser))).
		Methods(http.MethodPost)
	router.HandleFunc("/api/v1/remote/{id}", c.WithUserFunc(a.Get)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/remote/{id}", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Patch, auth.RoleSuperUser))).
		Methods(http.MethodPatch)
	router.HandleFunc("/api/v1/remote/{id}", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Delete, auth.RoleSuperUser))).
		Methods(http.MethodDelete)

	return a
}

// List godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary get all visible Remotes
// @Produce json
// @Success 200 {object} v1.Remotes
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote [get]
func (a *API) List(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("listing v1.Remotes")
	remotes, err := a.svc.List(r.Context())
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, remotes)
}

// ListByArchetype godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary get all visible Remotes
// @Produce json
// @Param arch query string true "Archetype"
// @Success 200 {object} v1.Remotes
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote/-/arch [get]
func (a *API) ListByArchetype(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	arch := r.URL.Query().Get("arch")
	if arch == "" {
		http.Error(w, i18n.ErrInvalidRequest, http.StatusBadRequest)
		return
	}
	log.WithFields(log.Fields{
		"id": id,
	}).Infof("listing v1.Remotes with arch: %s", arch)
	remotes, err := a.svc.ListByArchetype(r.Context(), arch)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, remotes)
}

// Get godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary get a Remote
// @Param id path string true "Remote to retrieve"
// @Success 200 {object} v1.Remote
// @Failure 400 {string} string "bad request"
// @Failure 404 {string} string "not found"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote/{id} [get]
func (a *API) Get(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	log.WithFields(log.Fields{
		"id": id,
	}).Info("retrieving v1.Remote")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	remote, err := a.svc.Get(r.Context(), objID.String())
	if ok := httputils2.ReturnGormErr(w, err); !ok {
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, remote)
}

// Create godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary create a Remote
// @Accept json
// @Produce json
// @Param request body api.CreateRemoteRequest true "Request payload"
// @Success 200 {object} v1.Remote
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote [post]
func (a *API) Create(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	log.WithFields(log.Fields{
		"id": id,
	}).Info("creating v1.Remote")
	var req api.CreateRemoteRequest
	if err := httputils.WithProtoBody(r, &req); err != nil {
		log.WithError(err).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	remote, err := a.svc.Create(r.Context(), &req)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, remote)
}

// Patch godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary patch a Remote
// @Accept json
// @Produce json
// @Param id path string true "Remote to retrieve"
// @Param request body api.CreateRemoteRequest true "Request payload"
// @Success 204 {string} string "no content"
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote/{id} [patch]
func (a *API) Patch(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	log.WithFields(log.Fields{
		"id": id,
	}).Info("patching v1.Remote")
	var req api.CreateRemoteRequest
	if err := httputils.WithProtoBody(r, &req); err != nil {
		log.WithError(err).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	err = a.svc.Patch(r.Context(), objID.String(), &req)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

// Delete godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.Remote
// @Summary delete a Remote
// @Param id path string true "Remote to delete"
// @Success 204 {string} string "no content"
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/remote/{id} [delete]
func (a *API) Delete(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("deleting v1.Remote")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(r.Context()).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	if err := a.svc.Delete(r.Context(), objID.String()); err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	// we have nothing to say
	w.WriteHeader(http.StatusNoContent)
}
