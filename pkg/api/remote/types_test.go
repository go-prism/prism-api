package remote_test

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/api/remote"
	"gitlab.com/go-prism/prism-api/pkg/cryptoutils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"testing"
)

var validTests = []struct {
	name string
	in   *api.CreateRemoteRequest
	ok   bool
}{
	{
		name: "Valid request",
		in: &api.CreateRemoteRequest{
			Name: "npm",
			Uri:  "https://registry.npmjs.org",
		},
		ok: true,
	},
	{
		name: "Missing name is invalid",
		in: &api.CreateRemoteRequest{
			Name: "",
			Uri:  "https://registry.npmjs.org",
		},
		ok: false,
	},
	{
		name: "Invalid URI is invalid",
		in: &api.CreateRemoteRequest{
			Name: "npm",
			Uri:  "foobar",
		},
		ok: false,
	},
	{
		name: "Invalid URI scheme is invalid",
		in: &api.CreateRemoteRequest{
			Name: "npm",
			Uri:  "ftp://registry.npmjs.org",
		},
		ok: false,
	},
}

func TestCreateRemoteRequest_IsValid(t *testing.T) {
	for _, tt := range validTests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.ok, remote.IsValid(tt.in))
		})
	}
}

//go:embed testdata/cert.crt
var cert string

var isValidCases = []struct {
	name string
	req  *api.UpdateProfileRequest
	err  error
}{
	{
		"empty request returns no error",
		&api.UpdateProfileRequest{},
		nil,
	},
	{
		"valid request returns no error",
		&api.UpdateProfileRequest{
			Ca:      cert,
			Cert:    cert,
			Key:     "this can be anything",
			SkipTLS: false,
		},
		nil,
	},
	{
		"invalid CA returns error",
		&api.UpdateProfileRequest{
			Ca:      "this is not a certificate",
			Cert:    cert,
			Key:     "this can be anything",
			SkipTLS: false,
		},
		cryptoutils.ErrNilBlock,
	},
	{
		"invalid cert returns error",
		&api.UpdateProfileRequest{
			Ca:      cert,
			Cert:    "this is not a certificate",
			Key:     "this can be anything",
			SkipTLS: false,
		},
		cryptoutils.ErrNilBlock,
	},
	{
		"missing cert returns no error",
		&api.UpdateProfileRequest{
			Ca:      cert,
			Cert:    "",
			Key:     "",
			SkipTLS: false,
		},
		nil,
	},
	{
		"missing key returns error",
		&api.UpdateProfileRequest{
			Ca:      "",
			Cert:    cert,
			Key:     "",
			SkipTLS: false,
		},
		remote.ErrCertButNoKey,
	},
}

func TestUpdateProfileRequest_IsValid(t *testing.T) {
	for _, tt := range isValidCases {
		t.Run(tt.name, func(t *testing.T) {
			assert.ErrorIs(t, remote.IsProfileValid(tt.req), tt.err)
		})
	}
}
