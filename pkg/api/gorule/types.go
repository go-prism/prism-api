package gorule

type CreateRewriteRule struct {
	Source      string `json:"source"`
	Destination string `json:"destination"`
}
