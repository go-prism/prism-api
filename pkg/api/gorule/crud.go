package gorule

import (
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	httputils2 "gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"go.opentelemetry.io/otel"
	"net/http"
)

type API struct {
	repos *repo.Repos
}

// NewAPI creates a new instance of API
// and attaches to the given mux.Router
func NewAPI(repos *repo.Repos, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	a := new(API)
	a.repos = repos

	router.HandleFunc("/api/v1/gorule", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.List, auth.RoleSuperUser))).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/gorule", c.WithUserFunc(roleSvc.
		WithRoleFunc(a.Create, auth.RoleSuperUser))).
		Methods(http.MethodPost)

	return a
}

// List godoc
// @Security AuthUser
// @Security AuthSource
// @Tags archv1.HostRewrite
// @Summary get all HostRewrites
// @Produce json
// @Success 200 {object} archv1.HostRewrites
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/gorule [get]
func (a *API) List(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_gorule_list")
	defer span.End()
	log.WithContext(ctx).Info("listing archv1.HostRules")
	rules, err := a.repos.HostRuleRepo.FindAll(ctx)
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, rules)
}

// Create godoc
// @Security AuthUser
// @Security AuthSource
// @Tags archv1.HostRewrite
// @Summary create a HostRewrite
// @Accept json
// @Produce json
// @Param request body CreateRewriteRule true "Request payload"
// @Success 200 {object} archv1.HostRewrite
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/gorule [post]
func (a *API) Create(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_gorule_create")
	defer span.End()
	log.WithContext(ctx).Info("creating archv1.HostRule")
	var request CreateRewriteRule
	if err := httputils.WithBody(r, &request); err != nil {
		log.WithError(err).WithContext(ctx).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	rule, err := a.repos.HostRuleRepo.Save(ctx, &archv1.HostRewrite{
		Id:          uuid.NewV4().String(),
		Source:      request.Source,
		Destination: request.Destination,
	})
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, rule)
}
