package gorule

import (
	"context"
	"errors"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"go.opentelemetry.io/otel"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type RPC struct {
	api.UnimplementedRulesServer
	repos *repo.Repos
}

func NewRPC(repos *repo.Repos) *RPC {
	r := new(RPC)
	r.repos = repos

	return r
}

func (r *RPC) Get(ctx context.Context, req *api.GetRuleRequest) (*archv1.HostRewrite, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_gorule_get")
	defer span.End()
	rule, err := r.repos.HostRuleRepo.FindBySource(ctx, req.GetSource())
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return rule, nil
}
