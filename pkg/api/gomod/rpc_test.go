package gomod_test

import (
	"gitlab.com/go-prism/prism-api/pkg/api/gomod"
	"gitlab.com/go-prism/prism-rpc/service/api"
)

// interface guard
var _ api.ModulesServer = &gomod.ModRPC{}
