package gomod

import (
	"context"
	"errors"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gorm.io/gorm"
)

type ModService struct {
	repos *repo.Repos
}

func NewModService(repos *repo.Repos) *ModService {
	svc := new(ModService)
	svc.repos = repos

	return svc
}

func (svc *ModService) List(ctx context.Context, name string) (*archv1.ModuleVersions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_mod_list", trace.WithAttributes(attribute.String("name", name)))
	defer span.End()
	return svc.repos.ModuleVersionRepo.FindAllByModule(ctx, name)
}

func (svc *ModService) Get(ctx context.Context, name, version string) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_mod_get", trace.WithAttributes(
		attribute.String("name", name),
		attribute.String("version", version),
	))
	defer span.End()
	return svc.repos.ModuleVersionRepo.FindByModuleVersion(ctx, name, version)
}

func (svc *ModService) Create(ctx context.Context, req *api.CreateModRequest) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_mod_create", trace.WithAttributes(
		attribute.String("name", req.GetName()),
		attribute.String("version", req.GetVersion()),
	))
	defer span.End()
	// only create the record if we don't already have one
	if existing, err := svc.repos.ModuleVersionRepo.FindByModuleVersion(ctx, req.GetName(), req.GetVersion()); err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	} else if existing != nil {
		// we found a record, so just send it back
		logrus.WithContext(ctx).Debugf("skipping creation of module version as it already exists")
		return existing, nil
	}
	// otherwise create a new one
	return svc.repos.ModuleVersionRepo.Save(ctx, &archv1.ModuleVersion{
		Id:          uuid.NewV4().String(),
		ModuleName:  req.GetName(),
		VersionName: req.GetVersion(),
		Timestamp:   req.GetTimestamp(),
	})
}
