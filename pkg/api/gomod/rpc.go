package gomod

import (
	"context"
	"errors"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"gorm.io/gorm"

	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ModRPC struct {
	api.UnimplementedModulesServer
	svc *ModService
}

func NewModRPC(svc *ModService) *ModRPC {
	r := new(ModRPC)
	r.svc = svc

	return r
}

func (r ModRPC) List(ctx context.Context, req *api.ListModRequest) (*archv1.ModuleVersions, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_mod_list")
	defer span.End()
	return r.svc.List(ctx, req.GetName())
}

func (r ModRPC) Get(ctx context.Context, req *api.GetModRequest) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_mod_get")
	defer span.End()
	mod, err := r.svc.Get(ctx, req.GetName(), req.GetVersion())
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}
	return mod, err
}

func (r ModRPC) Create(ctx context.Context, req *api.CreateModRequest) (*archv1.ModuleVersion, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_mod_create")
	defer span.End()
	return r.svc.Create(ctx, req)
}
