package core

import "net/http"

var (
	// HeaderAlwaysProxy indicates that the client
	// should ignore the METHOD and proxy the request
	// no matter what.
	HeaderAlwaysProxy = http.CanonicalHeaderKey("X-Prism-Always-Proxy")
)
