package providers

import (
	"context"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"net/http"
)

type StreamerFunc = func(ctx context.Context, w http.ResponseWriter, s reactor.Reactor_GetObjectV2Client)

type Base interface {
	IsMatch(path, method string) bool
	Do(ctx context.Context, w http.ResponseWriter, req *reactor.GetObjectRequest, resp StreamerFunc) (bool, error)
}
