package providers

import (
	"context"
	_ "embed"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestHelm_IsMatch(t *testing.T) {
	var cases = []struct {
		name     string
		path     string
		method   string
		expected bool
	}{
		{
			"root",
			"index.yaml",
			http.MethodGet,
			true,
		},
		{
			"subpath",
			"charts/index.yaml",
			http.MethodGet,
			true,
		},
		{
			"yml",
			"index.yml",
			http.MethodGet,
			false,
		},
		{
			"invalid method",
			"index.yaml",
			http.MethodPost,
			false,
		},
		{
			"suffix",
			"helmindex.yaml",
			http.MethodGet,
			false,
		},
	}

	h := &Helm{}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			ok := h.IsMatch(tt.path, tt.method)
			assert.EqualValues(t, tt.expected, ok)
		})
	}
}

//go:embed testdata/index.yaml
var indexFile string

//go:embed testdata/index-s3.yaml
var indexS3File string

//go:embed testdata/404.html
var htmlFile string

func TestHelm_parseYaml(t *testing.T) {
	h := NewHelm("", nil, nil)
	index, err := h.parse(context.TODO(), &indexResp{
		data: []byte(indexFile),
		ref:  "",
		src:  "",
	})
	assert.NoError(t, err)
	assert.Len(t, index.Entries, 2)
}

func TestHelm_parseNotYaml(t *testing.T) {
	h := NewHelm("", nil, nil)
	index, err := h.parse(context.TODO(), &indexResp{
		data: []byte(htmlFile),
		ref:  "",
		src:  "",
	})
	assert.Nil(t, index)
	assert.Error(t, err)
}

func TestHelm_rewrite(t *testing.T) {
	ctx := context.TODO()
	var cases = []struct {
		name     string
		index    string
		ref      string
		indexURL string
		expected string
	}{
		{
			"url is rewritten",
			indexFile,
			"tests",
			"https://technosophos.github.io/tscharts/index.yaml",
			"https://example.org/api/-/tests/alpine-0.2.0.tgz",
		},
		{
			"non-matching url is ignored",
			indexS3File,
			"tests",
			"https://technosophos.github.io/tscharts/index.yaml",
			"https://s3.amazonaws.com/tscharts/alpine-0.2.0.tgz",
		},
	}

	h := NewHelm("https://example.org", nil, nil)

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			index, err := h.parse(context.TODO(), &indexResp{
				data: []byte(tt.index),
				ref:  tt.ref,
				src:  tt.indexURL,
			})
			assert.NoError(t, err)

			h.rewrite(ctx, tt.ref, tt.indexURL, index)
			assert.EqualValues(t, tt.expected, index.Entries["alpine"][0].URLs[0])
		})
	}
}
