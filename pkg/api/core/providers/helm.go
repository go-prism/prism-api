package providers

import (
	"context"
	"fmt"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/ghodss/yaml"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"go.opentelemetry.io/otel"
	helmrepo "helm.sh/helm/v3/pkg/repo"
	"net/http"
	"path/filepath"
	"strings"
)

type indexResp struct {
	data []byte
	ref  string
	src  string
}

type Helm struct {
	publicURL string
	repos     *repo.Repos
	rec       reactor.ReactorClient
}

func NewHelm(publicURL string, repos *repo.Repos, rec reactor.ReactorClient) *Helm {
	h := new(Helm)
	h.publicURL = publicURL
	h.repos = repos
	h.rec = rec

	return h
}

func (*Helm) IsMatch(path, method string) bool {
	return method == http.MethodGet && (path == "index.yaml" || strings.HasSuffix(path, "/index.yaml"))
}

func (h *Helm) Do(ctx context.Context, w http.ResponseWriter, req *reactor.GetObjectRequest, resp StreamerFunc) (bool, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "core_provider_helm_do")
	defer span.End()
	log.WithContext(ctx).Debug("helm provider will attempt to multiplexed the index file")
	refraction, err := h.repos.RefractRepo.FindByName(ctx, req.GetRefraction())
	if err != nil {
		span.RecordError(err)
		return false, err
	}
	remotes, err := h.repos.RemoteRepo.FindAllByIDs(ctx, refraction.GetRemotes())
	if err != nil {
		span.RecordError(err)
		return false, err
	}
	data := make(chan *indexResp)
	log.WithContext(ctx).Debugf("preparing to fetch index.yaml from %d remotes", len(remotes.Remotes))
	for _, r := range remotes.GetRemotes() {
		go h.fetch(ctx, r, req, resp, data)
	}
	// create a new index and merge our fetched one's in
	index := helmrepo.NewIndexFile()
	var count int
	for count < len(remotes.Remotes) {
		elem := <-data
		// skip nil responses
		if elem == nil {
			log.WithContext(ctx).Debug("skipping nil response")
			count++
			continue
		}
		// try to parse this index file
		idx, err := h.parse(ctx, elem)
		if err != nil {
			count++
			continue
		}
		index.Merge(idx)
		count++
	}
	close(data)
	// sort the index since the merge has
	// likely jumbled it
	index.SortEntries()
	// convert to yaml and send it back
	body, err := yaml.Marshal(index)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to marshal merged index file")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return true, nil
	}
	resp(ctx, w, NewByteObjectV2Response(body))
	return true, nil
}

// rewrite modifies the index file to point back to Prism
// rather than at whatever backend the index file wanted.
//
// It will ignore URLs which can't be routed though the same
// Prism remote (e.g. S3)
func (h *Helm) rewrite(ctx context.Context, ref, indexURL string, index *helmrepo.IndexFile) {
	_, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "core_provider_helm_rewrite")
	defer span.End()
	prismPath := fmt.Sprintf("%s/api/-/%s/", h.publicURL, ref)
	root, _ := filepath.Split(indexURL)
	for _, e := range index.Entries {
		for _, v := range e {
			for i := range v.URLs {
				v.URLs[i] = strings.Replace(v.URLs[i], root, prismPath, 1)
			}
		}
	}
}

// parse converts the raw response data into a helmrepo.IndexFile.
// It uses the same YAML parser as ChartMuseum.
func (h *Helm) parse(ctx context.Context, resp *indexResp) (*helmrepo.IndexFile, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "core_provider_helm_parse")
	defer span.End()
	log.WithContext(ctx).Debug("attempting to parse raw index data")
	var index helmrepo.IndexFile
	if err := yaml.Unmarshal(resp.data, &index); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to unmarshal index file")
		span.RecordError(err)
		return nil, err
	}
	log.WithContext(ctx).Debugf("successfully parsed index with %d entries (%s)", len(index.Entries), index.Generated)
	h.rewrite(ctx, resp.ref, resp.src, &index)
	return &index, nil
}

func (h *Helm) fetch(ctx context.Context, remote *v1.Remote, req *reactor.GetObjectRequest, resp StreamerFunc, result chan *indexResp) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "core_provider_helm_fetch")
	defer span.End()
	log.WithContext(ctx).Infof("attempting to fetch index file: %s/%s", remote.GetUri(), req.GetPath())
	stream, err := h.rec.GetObjectV2(utils.SubmitCtx(ctx), &reactor.GetObjectRequest{
		Refraction: remote.GetName(),
		Path:       req.GetPath(),
		Method:     req.GetMethod(),
		Headers:    req.GetHeaders(),
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("got unexpected error from reactor")
		result <- nil
		return
	}
	buf := NewByteWriter()
	resp(ctx, buf, stream)
	log.WithContext(ctx).Infof("index retrieval responded with code: %d", buf.code)
	if httputils.IsHTTPSuccess(buf.code) {
		result <- &indexResp{
			data: buf.Bytes(),
			ref:  req.GetRefraction(),
			src:  fmt.Sprintf("%s/%s", remote.GetUri(), req.GetPath()),
		}
	} else {
		result <- nil
	}
}
