package providers

import (
	"bytes"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc"
	"io"
	"net/http"
)

// ByteWriter is a hack for faking a http.ResponseWriter
type ByteWriter struct {
	buf  *bytes.Buffer
	code int
}

func NewByteWriter() *ByteWriter {
	b := new(ByteWriter)
	b.buf = new(bytes.Buffer)

	return b
}

func (b *ByteWriter) Bytes() []byte {
	return b.buf.Bytes()
}

func (b *ByteWriter) Header() http.Header {
	return http.Header{}
}

func (b *ByteWriter) Write(i []byte) (int, error) {
	return b.buf.Write(i)
}

func (b *ByteWriter) WriteHeader(code int) {
	if b.code != 0 {
		return
	}
	b.code = code
}

// ByteObjectV2Response is a hack for faking a Reactor response
// using manufactured data.
//
// It implements the minimal functions needed to be used by the Core
// streamer, attempting to use others will panic.
type ByteObjectV2Response struct {
	data []byte
	grpc.ClientStream
	read bool
}

func NewByteObjectV2Response(data []byte) *ByteObjectV2Response {
	r := new(ByteObjectV2Response)
	r.data = data

	return r
}

func (r *ByteObjectV2Response) Recv() (*reactor.GetObjectV2Response, error) {
	if r.read {
		return nil, io.EOF
	}
	r.read = true
	return &reactor.GetObjectV2Response{
		Content: r.data,
		Code:    http.StatusOK,
	}, nil
}

func (r *ByteObjectV2Response) CloseSend() error {
	return nil
}
