package providers

import (
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"testing"
)

func TestByteWriter_WriteHeader(t *testing.T) {
	w := NewByteWriter()
	bw, err := w.Write([]byte("text"))
	assert.NoError(t, err)
	assert.EqualValues(t, 4, bw)
	assert.ElementsMatch(t, w.Bytes(), []byte("text"))

	w.WriteHeader(http.StatusOK)
	assert.EqualValues(t, http.StatusOK, w.code)
	w.WriteHeader(http.StatusNoContent)
	assert.EqualValues(t, http.StatusOK, w.code)
}

func TestByteObjectV2Response_Recv(t *testing.T) {
	txt := []byte("this is some text")
	r := NewByteObjectV2Response(txt)
	resp, err := r.Recv()
	assert.NoError(t, err)
	assert.ElementsMatch(t, resp.GetContent(), txt)
	assert.EqualValues(t, http.StatusOK, resp.GetCode())

	// ensure that the second read returns EOF
	resp, err = r.Recv()
	assert.Nil(t, resp)
	assert.ErrorIs(t, err, io.EOF)
}
