package core

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/api/core/providers"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"io"
	"net/http"
	"strings"

	"gitlab.com/go-prism/prism-api/pkg/api/extras"

	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
)

type API struct {
	rec          reactor.ReactorClient
	client       *http.Client
	helmProvider providers.Base
}

func NewAPI(publicURL string, repos *repo.Repos, rec reactor.ReactorClient, r *mux.Router) *API {
	api := new(API)
	api.rec = rec
	api.client = &http.Client{
		Transport: otelhttp.NewTransport(&http.Transport{
			TLSClientConfig: &tls.Config{
				MinVersion:               tls.VersionTLS12,
				PreferServerCipherSuites: true,
				CipherSuites: []uint16{
					tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
					tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				},
			},
			ForceAttemptHTTP2: true,
			Proxy:             http.ProxyFromEnvironment,
		}),
	}
	api.helmProvider = providers.NewHelm(publicURL, repos, rec)

	tracer.Apply(api.client)

	r.PathPrefix("/api/-/").Handler(api)

	return api
}

// RegisterPlugin attaches a given plugin to an
// api sub-path
func (api *API) RegisterPlugin(p *extras.Plugin, path string, router *mux.Router) {
	subPath := fmt.Sprintf("/api/plugin/-/%s", path)
	log.Infof("registering plugin to sub-path: '%s'", subPath)
	router.PathPrefix(subPath).HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "svc_core_plugin_serveHTTP", trace.WithAttributes(attribute.String("plugin", path)))
		defer span.End()
		r.URL.Path = fmt.Sprintf("/plugin%s", strings.TrimPrefix(r.URL.Path, subPath))
		p.ServeHTTP(w, r.Clone(ctx))
	})
}

func (api *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "svc_core_serveHTTP")
	defer span.End()
	root := strings.Replace(r.URL.Path, "/api/-/", "", 1)
	segs := strings.SplitN(root, "/", 2)
	// check that we're not going to panic by reading segments.
	// see - #6
	if len(segs) < 2 {
		log.WithContext(ctx).Error("invalid request, missing expected segments")
		http.Error(w, "a valid object must be requested", http.StatusBadRequest)
		return
	}
	ref := segs[0]
	path := segs[1]
	log.WithContext(ctx).Infof("proxy serving to refraction: %s, path: %s", ref, path)
	metricRequestMethod.WithLabelValues(r.Method).Inc()
	metricRefractRequest.WithLabelValues(ref).Inc()
	// remove forbidden headers
	r.Header.Del(HeaderAlwaysProxy)
	headers := httputils.HeadersAsFlatMap(&r.Header)
	req := &reactor.GetObjectRequest{
		Refraction: ref,
		Path:       path,
		Method:     r.Method,
		Headers:    headers,
	}
	if api.helmProvider.IsMatch(path, r.Method) {
		ok, err := api.helmProvider.Do(ctx, w, req, api.stream)
		if ok {
			span.AddEvent("helm provider responded")
			log.WithContext(ctx).Debug("helm provider has provided a response to this request")
			return
		}
		span.AddEvent("helm provider exited with non-ok or error")
		log.WithError(err).WithContext(ctx).Error("helm provider exited with non-ok or an error")
	}

	// allow any-method proxying for certain paths
	if strings.HasPrefix(path, "-/npm/v1/security/audits") {
		span.SetAttributes(attribute.Bool("always_proxy", true))
		headers[HeaderAlwaysProxy] = "true"
	}
	stream, err := api.rec.GetObjectV2(utils.SubmitCtx(ctx), req)
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("got unexpected error from reactor")
		// sniff the error for what should be gorm.ErrRecordNotFound
		if err.Error() == "rpc error: code = Unknown desc = record not found" {
			http.Error(w, i18n.ErrNotFound, http.StatusNotFound)
			return
		}
		http.Error(w, i18n.ErrGeneric, http.StatusInternalServerError)
		return
	}
	api.stream(ctx, w, stream)
}

func (*API) stream(ctx context.Context, w http.ResponseWriter, s reactor.Reactor_GetObjectV2Client) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_core_stream")
	defer span.End()
	// stream data back from the reactor
	wroteCode := false
	var code int32 = -1
	for {
		resp, err := s.Recv()
		if err != nil {
			// if we get EOF, we can safely stop
			if errors.Is(err, io.EOF) {
				break
			}
			// otherwise, halt and inform the requester
			span.RecordError(err)
			log.WithError(err).WithContext(ctx).Warning("got unexpected response reading from reactor")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		code = resp.GetCode()
		// write the http header if we got one
		// we must write the header early otherwise
		// writing the data will force it to 200 OK
		if code == 0 {
			log.WithContext(ctx).Warning("successfully proxied request, however we received no status code")
		} else if code > 0 && !wroteCode {
			log.WithContext(ctx).Debugf("writing status code: %d", code)
			w.WriteHeader(int(code))
			wroteCode = true
		}
		// write data directly to the response
		bw, err := w.Write(resp.GetContent())
		metricProxyResponseBytes.Add(float64(bw))
		log.WithError(err).WithContext(ctx).Debugf("wrote %d bytes from reactor with code %d", bw, code)
	}
	// send an ack
	if err := s.CloseSend(); err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to close reactor client stream")
	}
	metricReactorResponseCode.WithLabelValues(fmt.Sprintf("%d", code))
}
