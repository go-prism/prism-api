package core

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	metricRequestMethod = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "prism",
			Subsystem: "api",
			Name:      "core_request_method_total",
		},
		[]string{"method"},
	)
	metricProxyResponseBytes = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "core_response_bytes_total",
	})
	metricReactorResponseCode = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "reactor_response_code",
	}, []string{"code"})
	metricProxyResponseCode = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "proxy_response_code",
	}, []string{"code"})
	metricRefractRequest = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "api",
		Name:      "refract_request_total",
	}, []string{"name"})
)
