package reactor

import (
	"github.com/djcass44/go-utils/pkg/httputils"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"net/http"
)

// GetReactorStatus godoc
// @Security AuthUser
// @Security AuthSource
// @Tags Reactor
// @Summary get Reactor status
// @Produce json
// @Success 200 {object} v1.ReactorStatuses
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/reactor/-/status [get]
func (api *API) GetReactorStatus(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_reactor_getReactorStatus")
	defer span.End()
	status, err := api.repos.ReactorStatusRepo.FindAll(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to retrieve reactor status")
		http.Error(w, "failed to retrieve reactor status", http.StatusInternalServerError)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, status)
}
