package reactor

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/mitchellh/hashstructure/v2"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type API struct {
	v1.UnimplementedReactorServer
	repos *repo.Repos
	rc    reactor.ReactorClient
}

func NewAPI(repos *repo.Repos, rc reactor.ReactorClient, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	api := new(API)
	api.repos = repos
	api.rc = rc

	router.HandleFunc("/api/v1/reactor/-/status", c.WithUserFunc(roleSvc.
		WithRoleFunc(api.GetReactorStatus, auth.RoleSuperUser))).
		Methods(http.MethodGet)

	return api
}

func (api *API) Get(ctx context.Context, req *v1.GetContextRequest) (*v1.GetContextResponse, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_reactor_get")
	defer span.End()
	// fetch the Refraction by the given url
	refract, err := api.repos.RefractRepo.FindByName(ctx, req.GetKey())
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, err
	}
	// fetch the Remotes owned by this Refraction
	remotes, err := api.repos.RemoteRepo.FindAllByIDs(ctx, refract.Remotes)
	if err != nil {
		return nil, err
	}
	return &v1.GetContextResponse{
		Refraction: refract,
		Remotes:    remotes.Remotes,
	}, nil
}

func (api *API) GetConfig(ctx context.Context, _ *emptypb.Empty) (*v1.GetConfigResponse, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_reactor_getConfig")
	defer span.End()
	items := map[string]interface{}{}
	refractions, err := api.repos.RefractRepo.FindAll(ctx)
	if err != nil {
		log.WithError(err).Error("failed to fetch refractions")
		return nil, err
	}
	items["refractions"] = refractions
	remotes, err := api.repos.RemoteRepo.FindAll(ctx)
	if err != nil {
		log.WithError(err).Error("failed to fetch remotes")
		return nil, err
	}
	items["remotes"] = remotes
	hash, err := hashstructure.Hash(items, hashstructure.FormatV2, nil)
	if err != nil {
		log.WithError(err).Error("failed to generate hash")
		return nil, err
	}
	return &v1.GetConfigResponse{
		Hash: strconv.FormatUint(hash, 10),
	}, nil
}

// CreateStatus is used by Reactors to post information about their
// current status and configuration
func (api *API) CreateStatus(ctx context.Context, status *v1.ReactorStatus) (*emptypb.Empty, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_reactor_createStatus")
	defer span.End()
	_, err := api.repos.ReactorStatusRepo.Save(ctx, status)
	return &emptypb.Empty{}, err
}
