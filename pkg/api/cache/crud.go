package cache

import (
	"context"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"go.opentelemetry.io/otel"
	"net/http"

	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	httputils2 "gitlab.com/go-prism/prism-api/pkg/httputils"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
)

// API defines the CRUD endpoints
// for a v1.CacheEntry.
type API struct {
	v1.UnimplementedCacheServer
	repo      *repo.CacheEntryRepo
	sliceRepo *repo.CacheSliceRepo
}

// NewAPI creates a new instance of API
func NewAPI(repos *repo.Repos, router *mux.Router, c *client.Client) *API {
	api := new(API)
	api.repo = repos.CacheEntryRepo
	api.sliceRepo = repos.CacheSliceRepo

	router.HandleFunc("/api/v1/cache-entry/-/remote/{id}", c.WithUserFunc(api.ListByRemote)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/cache-entry/-/refract/{id}", c.WithUserFunc(api.ListByRefract)).
		Methods(http.MethodGet)

	return api
}

// ListByRemote godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.CacheEntry
// @Summary get all visible cache entries for a Remote.
// @Produce json
// @Param id path string true "Parent remote ID"
// @Success 200 {object} []v1.CacheEntry
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/cache-entry/-/remote/{id} [get]
func (api *API) ListByRemote(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_cache_listByRemote")
	defer span.End()
	api.list(w, r.Clone(ctx), api.repo.FindAllByRemoteID)
}

// ListByRefract godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.CacheEntry
// @Summary get all visible cache entries for a Refraction.
// @Produce json
// @Param id path string true "Parent refraction ID"
// @Success 200 {object} []v1.CacheEntry
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/cache-entry/-/refract/{id} [get]
func (api *API) ListByRefract(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_cache_listByRefract")
	defer span.End()
	api.list(w, r.Clone(ctx), api.repo.FindAllByRefractID)
}

func (api *API) list(w http.ResponseWriter, r *http.Request, f func(ctx context.Context, id string) (*v1.CacheEntries, error)) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "crud_cache_list")
	defer span.End()
	log.WithContext(ctx).Info("listing v1.CacheEntry")
	objID, err := uuid.FromString(mux.Vars(r)["id"])
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to parse 'id' as uuid")
		http.Error(w, "parameter 'id' must be a valid uuid", http.StatusBadRequest)
		return
	}
	entries, err := f(ctx, objID.String())
	if err != nil {
		httputils2.ReturnGormErr(w, err)
		return
	}
	httputils.ReturnProtoJSON(w, http.StatusOK, entries)
}
