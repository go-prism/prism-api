package cache

import (
	"context"
	"errors"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"go.opentelemetry.io/otel"
	"gorm.io/gorm"
)

func (api *API) Create(ctx context.Context, req *v1.CreateEntryRequest) (*v1.CreateEntryResponse, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_cache_create")
	defer span.End()
	log.WithContext(ctx).Info("creating v1.CacheEntry")
	// try to get the current download count, or default to 0
	entry, err := api.repo.FindByURI(ctx, req.GetUri())
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, err
		}
		log.WithContext(ctx).Info("failed to find existing v1.CacheEntry, creating a new one")
		entry = &v1.CacheEntry{
			Id:            uuid.NewV4().String(),
			Uri:           req.Uri,
			DownloadCount: 1,
			RemoteID:      req.GetRemoteID(),
			RefractID:     req.GetRefractID(),
		}
	} else {
		entry.DownloadCount++
	}
	// save the entry
	entry, err = api.repo.Save(ctx, entry)
	if err != nil {
		return nil, err
	}
	slice, err := api.sliceRepo.FindByEntryIDAndHash(ctx, entry.GetId(), req.GetHash())
	if err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, err
		}
		log.WithContext(ctx).Info("failed to find existing v1.CacheEntry, creating a new one")
		slice = &v1.CacheSlice{
			Id:      uuid.NewV4().String(),
			EntryID: entry.GetId(),
			Uri:     req.GetUri(),
			Hash:    req.GetHash(),
		}
	}
	// save the slice
	slice, err = api.sliceRepo.Save(ctx, slice)
	if err != nil {
		return nil, err
	}
	return &v1.CreateEntryResponse{
		Entry: entry,
		Slice: slice,
	}, nil
}
func (api *API) Delete(ctx context.Context, req *v1.DeleteEntryRequest) (*v1.DeleteEntryResponse, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "rpc_cache_delete")
	defer span.End()
	// delete the slices first
	log.WithContext(ctx).Info("deleting v1.CacheSlices")
	err := api.sliceRepo.DeleteByURI(ctx, req.GetUri())
	if err != nil {
		return nil, err
	}
	// delete the entry
	log.WithContext(ctx).Info("deleting v1.CacheEntry")
	err = api.repo.DeleteByURI(ctx, req.GetUri())
	if err != nil {
		return nil, err
	}
	return &v1.DeleteEntryResponse{
		Ok: true,
	}, nil
}
