package user

import (
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	"gitlab.com/av1o/cap10/pkg/client"
	"net/http"
)

type API struct{}

func NewAPI(router *mux.Router, c *client.Client) *API {
	a := new(API)

	router.HandleFunc("/api/v1/user/-/me", c.WithUserFunc(a.GetCurrent)).
		Methods(http.MethodGet)

	return a
}

// GetCurrent godoc
// @Security AuthUser
// @Security AuthSource
// @Tags user
// @Summary gets the current user
// @Success 200 {object} client.UserClaim
// @Failure 401 {string} string "unauthorised"
// @Router /api/v1/user/-/me [get]
func (*API) GetCurrent(w http.ResponseWriter, r *http.Request) {
	user, _ := client.GetRequestingUser(r)
	httputils.ReturnJSON(w, http.StatusOK, user)
}
