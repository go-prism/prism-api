package auth

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/av1o/cap10/pkg/verify"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

type fakeRolesRepo struct {
	repo.RolesRepo
}

// FindAllByUser retrieves the roles bound to
// the given user
func (r *fakeRolesRepo) FindAllByUser(_ context.Context, user string) ([]string, error) {
	switch user {
	case "oidc/jim.green":
		return []string{RolePowerUser}, nil
	case "oidc/joe.bloggs":
		return []string{RoleUser}, nil
	default:
		return nil, nil
	}
}

// Save creates or updates a given v1.Role
func (r *fakeRolesRepo) Save(_ context.Context, i *v1.RoleBinding) (*v1.RoleBinding, error) {
	return i, nil
}

func TestWithRole(t *testing.T) {
	var cases = []struct {
		name string
		req  *http.Request
		code int
	}{
		{
			"no user returns 401",
			&http.Request{},
			http.StatusUnauthorized,
		},
		{
			"user without role returns 403",
			&http.Request{
				Header: map[string][]string{
					client.DefaultSubjectHeader: {"<jacob.smith>"},
					client.DefaultIssuerHeader:  {"<oidc>"},
				},
			},
			http.StatusForbidden,
		},
		{
			"user with role returns handler code",
			&http.Request{
				Header: map[string][]string{
					client.DefaultSubjectHeader: {"<joe.bloggs>"},
					client.DefaultIssuerHeader:  {"<oidc>"},
				},
			},
			http.StatusNotFound,
		},
		{
			"user with incorrect role returns 403",
			&http.Request{
				Header: map[string][]string{
					client.DefaultSubjectHeader: {"<jim.green>"},
					client.DefaultIssuerHeader:  {"<oidc>"},
				},
			},
			http.StatusForbidden,
		},
	}

	svc := &Service{
		repos: &repo.Repos{
			RolesRepo: &fakeRolesRepo{},
		},
	}
	c := client.NewClient(verify.NewNoOpVerifier())
	handler := c.WithUser(svc.WithRole(http.NotFoundHandler(), RoleUser))

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			handler.ServeHTTP(w, tt.req)
			res := w.Result()
			_ = res.Body.Close()
			code := res.StatusCode
			assert.EqualValues(t, tt.code, code)
		})
	}
}
