package auth

import (
	"context"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"net/http"
)

const (
	// RoleUser is the default role given to all
	// and should give the least privilege
	RoleUser = "user"
	// RolePowerUser is a role given to users
	// with elevated but limited privilege
	RolePowerUser = "power_user"
	// RoleSuperUser is given to users that
	// have unlimited and full access to all
	// system controls and APIs
	RoleSuperUser = "super_user"
)

type Service struct {
	repos *repo.Repos
}

func NewService(repos *repo.Repos) *Service {
	svc := new(Service)
	svc.repos = repos

	return svc
}

func (svc *Service) WithRole(h http.Handler, roles ...string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !svc.withRole(w, r, roles...) {
			return
		}
		h.ServeHTTP(w, r)
	})
}

func (svc *Service) WithRoleFunc(f http.HandlerFunc, roles ...string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !svc.withRole(w, r, roles...) {
			return
		}
		f(w, r)
	}
}

func (svc *Service) withRole(w http.ResponseWriter, r *http.Request, requiredRoles ...string) bool {
	ok := svc.HasRole(r.Context(), requiredRoles...)
	if !ok {
		http.Error(w, i18n.ErrForbidden, http.StatusForbidden)
	}
	return ok
}

func (svc *Service) HasRole(ctx context.Context, requiredRoles ...string) bool {
	user, ok := client.GetContextUser(ctx)
	// check that we have a user
	if !ok || user == nil {
		log.WithContext(ctx).Debug("unable to check roles for missing user")
		return false
	}
	// lookup the users roles
	// todo - user roles should be cached to avoid the db round-trip
	roles, err := svc.repos.RolesRepo.FindAllByUser(ctx, user.AsUsername())
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to retrieve roles for user")
		return false
	}
	log.WithContext(ctx).Debugf("loaded %d roles for user: %+v", len(roles), roles)
	// check that the user has the required roles
	for _, role := range requiredRoles {
		if !sliceutils.Includes(roles, role) {
			log.WithContext(ctx).Warningf("rejecting user due to missing role '%s' in %+v", role, roles)
			return false
		}
	}
	return true
}
