package api

import (
	"context"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-rpc/service/api"
)

type API struct {
	api.UnimplementedRolesServer
}

func NewAPI() *API {
	svc := new(API)

	return svc
}

func (*API) GetRoles(context.Context, *api.GetRolesRequest) (*api.GetRolesResponse, error) {
	return &api.GetRolesResponse{
		Roles: []string{auth.RoleUser},
	}, nil
}
