package roles

import (
	httputils2 "github.com/djcass44/go-utils/pkg/httputils"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10/pkg/client"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
)

type API struct {
	repos   *repo.Repos
	roleSvc *auth.Service
}

func NewAPI(repos *repo.Repos, router *mux.Router, c *client.Client, roleSvc *auth.Service) *API {
	api := &API{
		repos:   repos,
		roleSvc: roleSvc,
	}

	router.HandleFunc("/api/v1/auth/-/roles", c.WithUserFunc(api.List)).
		Methods(http.MethodGet)
	router.HandleFunc("/api/v1/auth/-/role", c.WithUserFunc(roleSvc.WithRoleFunc(api.Create, auth.RoleSuperUser))).
		Methods(http.MethodPost)

	return api
}

// List godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.RoleBindings
// @Summary get all RoleBindings for a user
// @Produce json
// @Param user query string false "User ID"
// @Param offset query int false "Page offset"
// @Param size query int false "Page size"
// @Success 200 {object} v1.RoleBindings
// @Failure 400 {string} string "bad request"
// @Failure 403 {string} string "forbidden"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/auth/-/roles [get]
func (a *API) List(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("listing roles for user")
	// get request parameters
	offset, nok := httputils.GetInt(r.Context(), w, r.URL.Query().Get("offset"), 0)
	if nok {
		return
	}
	pageSize, nok := httputils.GetInt(r.Context(), w, r.URL.Query().Get("size"), 20)
	var f func() (*v1.RoleBindings, error)
	userID := r.URL.Query().Get("user")
	if userID == "" {
		if a.roleSvc.HasRole(r.Context(), auth.RoleSuperUser) {
			log.WithContext(r.Context()).Debugf("no user was specified, fetching all roles")
			f = func() (*v1.RoleBindings, error) {
				return a.repos.RolesRepo.FindAll(r.Context(), offset, pageSize)
			}
		} else {
			log.WithContext(r.Context()).Warningf("non super-users are not allowed to globally list roles")
			http.Error(w, i18n.ErrForbidden, http.StatusForbidden)
			return
		}
	} else {
		f = func() (*v1.RoleBindings, error) {
			return a.repos.RolesRepo.FindBindingsForUser(r.Context(), userID, offset, pageSize)
		}
	}
	roles, err := f()
	if !httputils.ReturnGormErr(w, err) {
		return
	}
	httputils2.ReturnProtoJSON(w, http.StatusOK, roles)
}

// Create godoc
// @Security AuthUser
// @Security AuthSource
// @Tags v1.RoleBindings
// @Summary create a RoleBinding
// @Produce json
// @Param request body CreateRoleBinding true "Request parameters"
// @Success 200 {object} v1.RoleBinding
// @Failure 400 {string} string "bad request"
// @Failure 500 {string} string "internal server error"
// @Router /api/v1/auth/-/role [post]
func (a *API) Create(w http.ResponseWriter, r *http.Request) {
	log.WithContext(r.Context()).Info("creating rolebinding")
	var req CreateRoleBinding
	if err := httputils2.WithBody(r, &req); err != nil {
		log.WithError(err).WithContext(r.Context()).Error(i18n.ErrParseBody)
		http.Error(w, i18n.ErrParseBody, http.StatusBadRequest)
		return
	}
	role, err := a.repos.RolesRepo.Save(r.Context(), &v1.RoleBinding{
		Id:       uuid.NewString(),
		Name:     req.Name,
		Username: req.Username,
		Subject:  req.Subject,
	})
	if !httputils.ReturnGormErr(w, err) {
		return
	}
	httputils2.ReturnProtoJSON(w, http.StatusOK, role)
}
