package roles

type CreateRoleBinding struct {
	Name     string `json:"name"`
	Subject  string `json:"subject"`
	Username string `json:"username"`
}
