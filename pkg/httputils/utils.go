package httputils

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/i18n"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

// PingFunc returns a 200 OK and logs
// some information about the requester.
//
// It is generally used for answering
// Kubernetes probes.
func PingFunc(w http.ResponseWriter, r *http.Request) {
	log.WithFields(log.Fields{
		"remoteAddr": r.RemoteAddr,
		"userAgent":  r.UserAgent(),
	}).Debug("answering probe")
	_, _ = w.Write([]byte("OK"))
}

// ReturnGormErr writes to the http.ResponseWriter
// based on the type of the given error.
//
// gorm.ErrRecordNotFound returns a 404, otherwise
// a 500 is returned.
//
// Returns `true` if there was no error
func ReturnGormErr(w http.ResponseWriter, err error) bool {
	if err == nil {
		return true
	}
	if errors.Is(err, gorm.ErrRecordNotFound) {
		http.Error(w, i18n.ErrNotFound, http.StatusNotFound)
	} else {
		http.Error(w, i18n.ErrGeneric, http.StatusInternalServerError)
	}
	return false
}

func GetInt(ctx context.Context, w http.ResponseWriter, val string, defaultValue int) (int, bool) {
	if val == "" {
		return defaultValue, false
	}
	num, err := strconv.Atoi(val)
	if err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to parse '%s' as int", val)
		http.Error(w, i18n.ErrInvalidRequest, http.StatusBadRequest)
		return 0, true
	}
	return num, false
}
