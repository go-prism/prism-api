package httputils_test

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-api/pkg/httputils"
	"gorm.io/gorm"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPingFunc(t *testing.T) {
	assert.HTTPSuccess(t, httputils.PingFunc, http.MethodGet, "http://localhost:8080/ping", nil)
}

var gormTests = []struct {
	err  error
	ok   bool
	code int
}{
	{nil, true, http.StatusOK},
	{gorm.ErrRecordNotFound, false, http.StatusNotFound},
	{errors.New("test"), false, http.StatusInternalServerError},
}

func TestReturnGormErr(t *testing.T) {
	for _, tt := range gormTests {
		t.Run(fmt.Sprintf("%+v", tt), func(t *testing.T) {
			w := httptest.NewRecorder()
			ok := httputils.ReturnGormErr(w, tt.err)
			assert.Equal(t, tt.ok, ok)

			res := w.Result()
			_ = res.Body.Close()
			assert.Equal(t, tt.code, res.StatusCode)
		})
	}
}
