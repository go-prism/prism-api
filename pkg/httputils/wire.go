package httputils

// TLSConfig defines the available TLS options
// that can be configured from the environment.
type TLSConfig struct {
	CertFile string `split_words:"true" ,default:""`
	KeyFile  string `split_words:"true" ,default:""`
	CaFile   string `split_words:"true" ,default:""`
}
