.PHONY: swagger
swagger:
	swag init --parseDependency --generalInfo ./cmd/api/main.go --output ./api

.PHONY: run
run:
	skaffold run -f ./deployments/skaffold.yaml

.PHONY: dev
dev:
	skaffold dev -f ./deployments/skaffold.yaml

.PHONY: test
test:
	scripts/test.sh