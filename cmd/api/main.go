package main

import (
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"gitlab.com/av1o/cap10/pkg/verify"
	"gitlab.com/go-prism/prism-api/internal/traceopts"
	"gitlab.com/go-prism/prism-api/pkg/api/gorule"
	"gitlab.com/go-prism/prism-api/pkg/api/roles"
	"gitlab.com/go-prism/prism-api/pkg/api/user"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	_ "go.uber.org/automaxprocs"
	"net/http"
	"os"

	"github.com/djcass44/go-tracer/tracer"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/cors"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/av1o/cap10/pkg/client"
	cap10 "gitlab.com/av1o/cap10/pkg/client"
	_ "gitlab.com/go-prism/prism-api/api"
	this "gitlab.com/go-prism/prism-api/pkg/api"
	"gitlab.com/go-prism/prism-api/pkg/api/auth"
	"gitlab.com/go-prism/prism-api/pkg/api/cache"
	"gitlab.com/go-prism/prism-api/pkg/api/core"
	"gitlab.com/go-prism/prism-api/pkg/api/extras"
	"gitlab.com/go-prism/prism-api/pkg/api/gomod"
	"gitlab.com/go-prism/prism-api/pkg/api/probe"
	"gitlab.com/go-prism/prism-api/pkg/api/reactor"
	"gitlab.com/go-prism/prism-api/pkg/api/refract"
	"gitlab.com/go-prism/prism-api/pkg/api/remote"
	"gitlab.com/go-prism/prism-api/pkg/dao"
	"gitlab.com/go-prism/prism-api/pkg/dao/repo"
	"gitlab.com/go-prism/prism-api/pkg/flag"
	"gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/jobs"
	"gitlab.com/go-prism/prism-api/pkg/leader"
	"gitlab.com/go-prism/prism-api/pkg/logging"
	metrics2 "gitlab.com/go-prism/prism-api/pkg/metrics"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/rpclient"
	"gitlab.com/go-prism/prism-rpc/pkg/rpcserv"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	reactor2 "gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

type environment struct {
	Port      int `default:"8080"`
	Log       logging.Config
	Flag      flag.Config
	ServerTLS httputils.TLSConfig `split_words:"true"`
	ClientTLS httputils.TLSConfig `split_words:"true"`

	DSN string `required:"true"`

	AdminUser     string `split_words:"true"`
	EnableProbing bool   `split_words:"true" default:"false"`
	EnableTracing bool   `split_words:"true" default:"false"`

	ReactorURL       string `default:"localhost:8081" split_words:"true"`
	PluginGoProxyURL string `split_words:"true"`
	PublicURL        string `default:"https://prism.devel" split_words:"true"`

	HTTP struct {
		UserHeader       string `split_words:"true" default:"X-Auth-User"`
		SourceHeader     string `split_words:"true" default:"X-Auth-Source"`
		VerifyHeader     string `split_words:"true" default:"X-Auth-Verify"`
		VerifyHashHeader string `split_words:"true" default:"X-Auth-Hash-Verify"`
	}
	CAP10 struct {
		Method string `split_words:"true" default:"http"`
		URL    string `split_words:"true" default:"url"`
		Path   string `split_words:"true" default:"./ca.crt"`
	}
	Environment string `split_words:"true" envconfig:"GITLAB_ENVIRONMENT_NAME"`
}

// @title Prism API
// @description The HTTP API component of the Prism package caching service.
// @version 0.1

// @license.name Apache 2.0
// @license.url https://www.apache.org/licenses/LICENSE-2.0

// @securityDefinitions.apiKey AuthUser
// @in header
// @name X-Auth-User

// @securityDefinitions.apiKey AuthSource
// @in header
// @name X-Auth-Source
func main() {
	// read config
	var e environment
	err := envconfig.Process("prism", &e)
	if err != nil {
		log.WithError(err).Fatal("failed to read environment")
		return
	}

	// configure logging
	logging.Init(&e.Log)
	log.Debugf("starting with config: %+v", e)

	// set the extractor headers
	cap10.DefaultSubjectHeader = e.HTTP.UserHeader
	cap10.DefaultIssuerHeader = e.HTTP.SourceHeader
	cap10.DefaultVerifyHeader = e.HTTP.VerifyHeader
	cap10.DefaultVerifyHashHeader = e.HTTP.VerifyHashHeader

	// setup otel
	if e.EnableTracing {
		if err := tracing.Init(tracing.OtelOptions{
			ServiceName: traceopts.DefaultServiceName,
			Environment: e.Environment,
		}); err != nil {
			log.WithError(err).Error("failed to setup tracing")
			return
		}
	}

	// setup services
	accessLayer, err := dao.NewAccessLayer(e.DSN)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to database")
		return
	}
	if err := accessLayer.Init(); err != nil {
		log.WithError(err).Fatal("failed to initialise database")
		return
	}
	accessLayer.OnStart(func(db *gorm.DB) {
		createAdmin(db, e.AdminUser)
	})
	repos := repo.Init(accessLayer)
	roleSvc := auth.NewService(repos)

	// setup clients
	sc, err := rpclient.NewAutoConn(e.ReactorURL, e.ClientTLS.CertFile, e.ClientTLS.KeyFile, e.ClientTLS.CaFile,
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
	)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to reactor")
		return
	}
	reactorClient := reactor2.NewReactorClient(sc)

	// set up the auth verifier
	var verifier cap10.BaseVerifier
	switch e.CAP10.Method {
	case verify.ProviderLocal:
		log.Info("initialising cap10 file verifier")
		verifier, err = verify.NewFileVerifier(e.CAP10.Path)
	case verify.ProviderHTTP:
		log.Info("initialising cap10 http verifier")
		verifier, err = cap10.NewVerifier(e.CAP10.URL)
	case verify.ProviderNone:
		log.Warning("initialising cap10 no-op verifier - this disables request forgery protection")
		verifier = verify.NewNoOpVerifier()
		err = nil
	}
	if err != nil {
		log.WithError(err).Fatal("failed to initialise cap10 verifier")
		return
	}

	c := client.NewClient(verifier)

	// setup routes
	router := mux.NewRouter()
	router.Use(tracer.NewHandler)
	router.Use(otelmux.Middleware(traceopts.DefaultServiceName))
	metrics2.Init(router, "/api/metrics")
	// enable swagger page
	router.PathPrefix("/api/swagger").Handler(httpSwagger.WrapHandler).Methods(http.MethodGet)
	// enable k8s probes
	router.HandleFunc("/api/v1/ping", httputils.PingFunc)

	_ = remote.NewAPI(repos, router, c, roleSvc)
	_ = refract.NewAPI(repos, reactorClient, router, c, roleSvc)
	coreAPI := core.NewAPI(e.PublicURL, repos, reactorClient, router)
	_ = probe.NewAPI(repos, router, c, roleSvc)
	_ = gorule.NewAPI(repos, router, c, roleSvc)
	_ = roles.NewAPI(repos, router, c, roleSvc)
	_ = user.NewAPI(router, c)
	probeJob := jobs.NewProbeJob(repos, e.EnableProbing)

	// register plugins
	if e.PluginGoProxyURL != "" {
		goPlugin, err := extras.NewPlugin(e.PluginGoProxyURL)
		if err != nil {
			log.WithError(err).Fatal("failed to configure GoProxy plugin")
			return
		}
		coreAPI.RegisterPlugin(goPlugin, "mod", router)
	} else {
		log.Info("skipping GoProxy plugin initialisation")
	}

	// start background jobs
	var leadingJobs = []jobs.BaseRunner{probeJob}
	var standardJobs []jobs.BaseRunner
	if e.Flag.URL != "" {
		wd, err := flag.NewWatchdog(&e.Flag)
		if err != nil {
			log.WithError(err).Fatal("failed to setup unleash watchdog")
		} else {
			standardJobs = append(standardJobs, wd)
		}
	}

	// start the event loop
	runner := &jobs.Runner{}
	runner.Init(standardJobs, leadingJobs)
	// we can only execute background jobs if we're the elected leader
	candidate, err := leader.NewK8SCandidate(leader.InClusterLoader, "prism-api-lock", os.Getenv("KUBERNETES_NAMESPACE"))
	if err != nil {
		log.WithError(err).Error("failed to setup leader election - background jobs will not run")
	} else {
		// start async so we're not blocking main execution
		go candidate.GetLock(runner)
	}

	// start the server
	gsrv := grpc.NewServer(utils.GetOpts(
		[]grpc.StreamServerInterceptor{otelgrpc.StreamServerInterceptor()},
		[]grpc.UnaryServerInterceptor{otelgrpc.UnaryServerInterceptor()},
	)...)
	v1.RegisterCacheServer(gsrv, cache.NewAPI(repos, router, c))
	v1.RegisterReactorServer(gsrv, reactor.NewAPI(repos, reactorClient, router, c, roleSvc))
	api.RegisterRolesServer(gsrv, this.NewAPI())
	api.RegisterRemotesServer(gsrv, remote.NewRPC(remote.NewService(repos)))
	api.RegisterRefractionsServer(gsrv, refract.NewRPC(refract.NewService(repos, reactorClient)))
	api.RegisterModulesServer(gsrv, gomod.NewModRPC(gomod.NewModService(repos)))
	api.RegisterRulesServer(gsrv, gorule.NewRPC(repos))

	srv, err := rpcserv.NewDualPurpose(e.Port, gsrv, cors.AllowAll().Handler(router))
	if err != nil {
		log.WithError(err).Fatal("failed to open listener")
		return
	}
	log.Fatal(srv.ListenAndServeAuto(e.ServerTLS.CertFile, e.ServerTLS.KeyFile, e.ServerTLS.CaFile))
}

func createAdmin(db *gorm.DB, user string) {
	log.Debugf("ensuring that initial admin role exists for user: '%s'", user)
	if err := db.Where("name = ? AND username = ?", auth.RoleSuperUser, user).FirstOrCreate(&v1.RoleBindingORM{
		Id:       uuid.NewV4().String(),
		Name:     auth.RoleSuperUser,
		Username: user,
	}).Error; err != nil {
		log.WithError(err).Error("failed to initialise admin RoleBinding, this may cause auth issues")
	}
}
