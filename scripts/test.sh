#!/usr/bin/env bash

set -e

echo "Running Go tests..."

go test ./...